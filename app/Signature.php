<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Signature extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'signature';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'representation', 'function_option'];

    /********** Voyager relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function workspaceId()
    {
        return $this->belongsTo(\App\Workspace::class);
    }
    /********** Voyager relations **********/

    /********** Relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function workspace()
    {
        return $this->workspaceId();
    }
    /********** Relations **********/
}
