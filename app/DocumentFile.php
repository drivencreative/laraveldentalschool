<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class DocumentFile extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'documentfile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['version', 'content', 'additional_content'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['signed'];

    /********** Voyager relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function documentId()
    {
        return $this->belongsTo(\App\Document::class);
    }
    /********** Voyager relations **********/

    /********** Relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function downloadLogs()
    {
        return $this->hasMany(\App\DownloadLog::class, 'document_id');
    }
    /********** Relations **********/

    /********** Download Log **********/
    public function getIsNewAttribute()
    {
        return !$this->downloadLogs()->count() && $this->signed && !Auth::user()->hasPermission('edit_documents');
    }
    /********** Download Log **********/
}
