<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'workspace';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','street','zip','city','land','terms','master','dental_assistant','medical_officers','medical_device','security_officers','laser_protection','leading_zfa','leading_zfa_representative','zfas','zfas_representative','reception','reception_representation','accounting','billing_representative','practice_laboratory','practice_laboratory_representative','staff','personal_representation','financial_accounting','financial_accounting_representative','purchase','shopping_representation','it_organization','it_organization_representative'];

    /********** Service **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     **/
    public function contract()
    {
        return $this->morphToMany(\App\Contract::class, 'service', 'services', 'service_id', 'contract_id');
    }
    /********** Service **********/

    /********** Users **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function users()
    {
        return $this->morphToMany(\App\User::class, 'user_service', 'user_services', 'user_service_id', 'user_id');
    }
    /********** Users **********/

    /********** Documents **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(\App\Document::class);
    }
    /********** Documents **********/

    /********** Signature **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function signature()
    {
        return $this->hasMany(\App\Signature::class);
    }
    /********** Signature **********/

    /********** Devices **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(\App\Device::class);
    }
    /********** Devices **********/

    public function setTermsAttribute($value)
    {
        $this->attributes['terms'] = boolval($value) ?: false;
    }
    public function setMasterAttribute($value)
    {
        $this->attributes['master'] = boolval($value) ?: false;
    }

    /********** Voyager BREAD **********/
    /**
     * @param array $data
     * @return mixed
     */
    public function save_users($data)
    {
        return $this->users()->sync($data);
    }
    /********** Voyager BREAD **********/
}
