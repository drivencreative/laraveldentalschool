<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /********** Voyager relations **********/
    public function roleId()
    {
        return $this->role();
    }
    /********** Voyager relations **********/

    /********** Service **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphToMany
     **/
    public function workspace()
    {
        return $this->morphedByMany(\App\Workspace::class, 'user_service');
    }

    public function getWorkspaceAttribute($value = '')
    {
        return $this->workspace()->first();
    }
    /********** Service **********/

}
