<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'parent_id'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['subCategories'];

    /********** SubCategories **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function subCategories()
    {
        return $this->hasMany(\App\Category::class, 'parent_id');
    }

    public function subCategoriesIds($categories = null)
    {
        $categories = $categories ?: $this->subCategories;

        $ids = [];
        foreach ($categories as $category) {
            if ($category->subCategories) {
                $ids = array_merge($ids, $this->subCategoriesIds($category->subCategories));
            }
            $ids[] = $category->id;
        }

        return $ids;
    }
    /********** SubCategories **********/
}
