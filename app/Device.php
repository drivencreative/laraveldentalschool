<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Device extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'workspace_id', 'type', 'name', 'manufacturer', 'ce_mark', 'serial_number', 'year_of_construction', 'space', 'number', 'notes'];

    /********** Override **********/
    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        if (Auth::user()->workspace && !isset($attributes['workspace_id'])) {
            $attributes['workspace_id'] = Auth::user()->workspace->id;
        }
        return parent::fill($attributes);
    }
    /********** Override **********/

    /********** Voyager relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function workspaceId()
    {
        return $this->belongsTo(\App\Workspace::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function categoryId()
    {
        return $this->belongsTo(\App\Category::class, 'category_id', 'id');
    }
    /********** Voyager relations **********/

    /********** Relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function workspace()
    {
        return $this->workspaceId();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->categoryId();
    }
    /********** Relations **********/

    /********** Clone **********/
    public function clone() {
        $clone = $this->replicate();
        $clone->workspace_id = Auth::user()->workspace->id;
        $clone->save();

        return $clone;
    }
    /********** Clone **********/

    /********** Generator **********/
    public function getGeneratorAttribute($value = '') {
        return (new \App\Device($this));
    }
    /********** Generator **********/

}
