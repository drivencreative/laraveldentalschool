<?php

/**
 * @param string $content
 * @param string $name
 * @param string $extension
 * @return string
 */
function temporaryFile($content, $name, $extension = '.html'){
    $filename = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $name . $extension;

    if ($content) {
        file_put_contents($filename, $content);
    }

    return $filename;
}