<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class WorkspaceContract
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date = new \DateTime();
        // Check for valid contract
        if (Auth::user()->workspace && Auth::user()->workspace->contract()->where('valid_from', '<', $date)->where('valid_to', '>', $date)->count()) {
            // Editor permissions
            if (Auth::user()->hasPermission('edit_documents')) {
                // Check Workspace terms and condition
                if (!$request->route()->named('workspace.*') && validator(Auth::user()->workspace->toArray(), ['terms' => 'required|accepted'])->fails()) {
                    return redirect(route('workspace.index'))->with('status', __('workspace.accept_terms'));
                }
                // Check if documents are initialized
                if ($request->route()->named('document.*') && !(Auth::user()->workspace->master || Auth::user()->workspace->initialized)) {
                    return redirect(route('initialize.create'));
                }
            } else if (
                $request->route()->named('workspace.*') ||
                $request->route()->named(['document.create', 'document.store', 'document.edit', 'document.update', 'version.*', 'sign.*', 'notification.*', 'initialize.*']) ||
                $request->route()->named(['device.create', 'device.store', 'device.edit', 'device.update'])
            ) {
                // Viewer is not allowed
                abort(403, 'Access denied');
            }
            // Check if documents are already initialize
            if ($request->route()->named('initialize.*') && (Auth::user()->workspace->master || Auth::user()->workspace->initialized)) {
                return redirect(route('document.index'));
            }
            return $next($request);
        }
        abort(403, 'Access denied');
    }
}
