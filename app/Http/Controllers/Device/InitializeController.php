<?php

namespace App\Http\Controllers\Device;

use App\Workspace;
use Illuminate\Support\Facades\Auth;

class InitializeController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->workspace->devices()->count()) {
            return redirect(route('device.index'));
        }

        $devices = Workspace::where('master', true)->first()->devices->map->clone();

        $devices->groupBy('category.id')->each(function ($devices, $category) {
            app(\App\Generator\Device::class)->create($devices, $category);
        });

        return redirect(route('device.index'));
    }
}
