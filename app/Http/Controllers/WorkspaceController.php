<?php

namespace App\Http\Controllers;

use App\Workspace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WorkspaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('workspace.index')->withWorkspace(Auth::user()->workspace->load(['signature']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Workspace $workspace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workspace $workspace)
    {
        $workspace->load(['signature']);

        $data = $request->validate([
            'name' => 'required|string|max:255',
            'terms' => 'required|accepted',
            'dental_assistant' => 'sometimes',
            'medical_officers' => 'required',
            'medical_device' => 'required',
            'security_officers' => 'required',
            'laser_protection' => 'required',
            'leading_zfa' => 'required',
            'leading_zfa_representative' => 'sometimes',
            'zfas' => 'sometimes',
            'zfas_representative' => 'sometimes',
            'reception' => 'sometimes',
            'reception_representation' => 'sometimes',
            'accounting' => 'sometimes',
            'billing_representative' => 'sometimes',
            'practice_laboratory' => 'sometimes',
            'practice_laboratory_representative' => 'sometimes',
            'staff' => 'sometimes',
            'personal_representation' => 'sometimes',
            'financial_accounting' => 'sometimes',
            'financial_accounting_representative' => 'sometimes',
            'purchase' => 'sometimes',
            'shopping_representation' => 'sometimes',
            'it_organization' => 'sometimes',
            'it_organization_representative' => 'sometimes',
            'street' => 'required|string|max:255',
            'zip' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'land' => 'required|string|max:255',
            'signature.*.name' => 'required|string|max:255',
            'signature.*.representation' => 'sometimes',
            'signature.*.function_option' => 'sometimes',
            'signature.*.file' => 'sometimes|image',
        ]);

        $workspace->fill($data);

        if ($workspace->signature->count()) {
            foreach ($workspace->signature as $key => &$signature) {
                $signature->fill($data['signature'][$key]);
            }
        } else {
            $workspace->signature()->createMany($data['signature']);
        }

        if ($request->hasFile('signature.*.file')) {
            foreach ($workspace->signature as $key => $signature) {
                if ($request->hasFile("signature.$key.file")) {
                    if ($signature->hasMedia('signature')) {
                        $signature->clearMediaCollection('signature');
                    }
                    $signature->addMedia($request->file("signature.$key.file"))->toMediaCollection('signature');
                }
            }
        }

        if ($workspace->isDirty('terms')) {
            \App\TermsLog::logged();
        }

        $workspace->push();

        if ($workspace->initialized || $workspace->master) {
            \App\Generator\Document\Hygieneplan::create($workspace);
            \App\Generator\Document\QMHandbuch::create($workspace, \App\Generator\Document\Organigramm::create($workspace));
        }

        return redirect(route('workspace.index'));
    }
}
