<?php

namespace App\Http\Controllers;

use App\Device;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $devices = Auth::user()->workspace->devices();

        if (!Auth::user()->workspace->master && Auth::user()->hasPermission('edit_documents') && !$devices->count()) {
            return redirect(route('device.initialize.create'));
        }

        if ($request->has('category')) {
            $devices = $devices->where('category_id', $request->category);
        }

        return view('device.index', ['devices' => $devices->paginate(config('document.pagination'))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('device.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'manufacturer' => 'required|string|max:255',
        ]);

        $device = app(\App\Device::class)->fill($request->all());
        $device->save();

        app(\App\Generator\Device::class)->create(null, $device->category_id);

        return redirect(route('device.index', ['category' => $device->category_id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Device $device
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        return view('device.edit')->withDevice($device);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Device $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Device $device)
    {
        $request->validate([
            'type' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'manufacturer' => 'required|string|max:255',
        ]);

        $device->fill($request->all());
        $device->save();

        app(\App\Generator\Device::class)->create(null, $device->category_id);

        return redirect(route('device.index', ['category' => $device->category_id]));
    }

}
