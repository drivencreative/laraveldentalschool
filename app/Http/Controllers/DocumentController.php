<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $documents = Auth::user()->workspace->documents()->with('documentFiles');
        if ($request->has('category')) {
            $documents = $documents->where('category_id', $request->category);
        }
        if (!Auth::user()->hasPermission('edit_documents')) {
            $documents = $documents->has('signedDocumentFiles');
        }
        return view('document.index', ['documents' => $documents->paginate(config('document.pagination'))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('document.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $document = app(\App\Document::class)->fill($request->all());
        $documentFile = app(\App\DocumentFile::class)->fill($request->all());

        $request->validate($document->generator->rules());

        if ($request->has('file')) {
            $documentFile->addMedia($request->file)->toMediaCollection('file');
        }

        $document->save();
        $document->documentFiles()->save($documentFile);

        return redirect(route('document.index', ['category' => $request->category_id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        $document->logged();

        return $document->generator->generate();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Document $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        return view('document.edit')->withDocument($document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Document $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        $document->fill($request->all());
        $documentFile = $document->newest_document_file->fill($request->all());

        $request->validate($document->generator->rules('update'));

        if ($request->has('file')) {
            $documentFile->addMedia($request->file)->toMediaCollection('file');
        }

        $document->save();
        $documentFile->save();

        return redirect(route('document.index', ['category' => $document->category_id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Document $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        // $document->delete();
    }

}
