<?php

namespace App\Http\Controllers\Document;

use App\Workspace;
use Illuminate\Support\Facades\Auth;

class InitializeController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Workspace::where('master', true)->first()->documents->load('documentFiles')->each->clone();

        $workspace = Auth::user()->workspace;
        $workspace->initialized = true;
        $workspace->save();

        \App\Generator\Document\Hygieneplan::create($workspace);
        \App\Generator\Document\QMHandbuch::create($workspace, \App\Generator\Document\Organigramm::create($workspace));

        return redirect(route('document.index'));
    }
}
