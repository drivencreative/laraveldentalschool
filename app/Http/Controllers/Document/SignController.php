<?php

namespace App\Http\Controllers\Document;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignController extends \App\Http\Controllers\DocumentController
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $documents = Auth::user()->workspace->documents()->with('documentFiles')->has('pendingDocumentFiles');

        return view('document.index', ['documents' => $documents->paginate(config('document.pagination'))]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Document $sign
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $sign)
    {
        return view('document.sign.edit')->withDocument($sign);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Document $sign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $sign)
    {
        $request->validate([
            'sign' => 'required|numeric|min:1'
        ]);

        $signatures = Auth::user()->workspace->signature->where('id', $request->sign);
        $signature = $signatures->first();

        $documentFile = $sign->newest_document_file;
        $documentFile->signed = new \DateTime();
        $documentFile->signature_index = $signatures->keys()->first();
        $documentFile->signature = $signature->name;
        if ($signature->hasMedia('signature')) {
            $documentFile->copyMedia($signature->getFirstMedia('signature')->getPath())->toMediaCollection('signature');
        }

        $documentFile->save();

        return redirect(route('sign.index'));
    }

}
