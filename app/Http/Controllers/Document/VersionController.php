<?php

namespace App\Http\Controllers\Document;

use App\Document;
use Illuminate\Http\Request;

class VersionController
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  Document $version
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $version)
    {
        return view('document.version.edit')->withDocument($version);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Document $version
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $version)
    {
        $request->validate($version->generator->rules());

        $version->fill($request->all());
        if ($version->based_on) {
            $version->based_on_version = $version->based_on->newest_document_file->version;
        }

        $originalDocumentFile = $version->newest_document_file;
        $documentFile = $originalDocumentFile->fill($request->all())->replicate();
        $documentFile->version++;
        unset($documentFile->signed, $documentFile->signature);

        $version->save();
        $documentFile->save();

        Document::where('based_on', $version->id)->update(['notify' => true]);

        return redirect(route('document.index', ['category' => $version->category_id]));
    }
}
