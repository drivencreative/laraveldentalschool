<?php

namespace App\Http\Controllers\Document;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends \App\Http\Controllers\DocumentController
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $documents = Auth::user()->workspace->documents()->with('documentFiles')->where('notify', true);

        return view('document.index', ['documents' => $documents->paginate(config('document.pagination'))]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Document $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $notification)
    {
        return view('document.notification.edit')->withDocument($notification->load('basedOn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Document $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $notification)
    {
        if ($request->clone) {
            $notification->fill($notification->basedOn->only('name', 'based_on_version'));
            $documentFile = $notification->newest_document_file->fill($notification->basedOn->newest_document_file->only('content', 'additional_content', 'signed', 'signature'));

            if ($notification->basedOn->newest_document_file->hasMedia('file')) {
                $documentFile->clearMediaCollection('file');
                $documentFile->copyMedia($notification->basedOn->newest_document_file->getFirstMedia('file')->getPath())->toMediaCollection('file');
            }
        } else {
            $notification->fill($request->all());
            $documentFile = $notification->newest_document_file->fill($request->all());
        }
        $notification->notify = false;

        $notification->save();
        $documentFile->save();

        return redirect(route('notification.index'));
    }

}
