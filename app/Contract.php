<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contract';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id', 'valid_from', 'valid_to', 'bill_date', 'cost'];

    /********** Voyager relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function customerId()
    {
        return $this->belongsTo(\App\Customer::class);
    }
    public function customer()
    {
        return $this->customerId();
    }
    /********** Voyager relations **********/

    /********** Voyager BREAD **********/
    /**
     * @param array $data
     * @return mixed
     */
    public function save_workspace($data)
    {
        return $this->workspace()->sync($data);
    }
    /********** Voyager BREAD **********/

    /********** Service **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphToMany
     **/
    public function workspace()
    {
        return $this->morphedByMany(\App\Workspace::class, 'service');
    }
    /********** Service **********/

    public function setCustomerIdAttribute($value)
    {
        if ($value) {
            $this->attributes['customer_id'] = intval($value);
        }
    }
}
