<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    public function getFileAttribute($value = '')
    {
        return $value ? optional(current(json_decode($value)))->download_link : '';
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        //when an item is created
        static::created(function (Term $term) {
            Workspace::query()->update(['terms' => 0]);
        });

        //when an item is updated
        static::updated(function (Term $term) {
            Workspace::query()->update(['terms' => 0]);
        });

        //when item deleted
        static::deleted(function (Term $term) {
            Workspace::query()->update(['terms' => 0]);
        });
    }
}
