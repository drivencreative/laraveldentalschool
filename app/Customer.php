<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /********** Address **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     **/
    public function address()
    {
        return $this->morphMany(\App\Address::class, 'record');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     **/
    public function billing(Address $address)
    {
        $address->type = 'billing';

        return $this->address()->save($address);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Model
     **/
    public function contact(Address $address)
    {
        $address->type = 'contact';

        return $this->address()->save($address);
    }

    public function getBillingAttribute($value = '')
    {
        return $this->address->filter(function ($value) {
            return $value->type == 'billing';
        });
    }

    public function getContactAttribute($value = '')
    {
        return $this->address->filter(function ($address) {
            return $address->type == 'contact';
        });
    }
    /********** Address **********/

    /********** Contract **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     **/
    public function contract()
    {
        return $this->hasMany(\App\Contract::class);
    }
    /********** Contract **********/

}
