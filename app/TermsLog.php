<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class TermsLog extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'terms_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['workspace_id', 'user_id'];

    /**
     * Create the Term Log.
     *
     * @return TermsLog
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public static function logged()
    {
        $log = static::create([
            'user_id' => Auth::user()->id,
            'workspace_id' => Auth::user()->workspace->id
        ]);
        $log->copyMedia('storage/' . \App\Term::orderBy('updated_at', 'desc')->first()->file)->toMediaCollection('terms');

        return $log;
    }

}
