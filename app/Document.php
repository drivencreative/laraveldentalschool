<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'workspace_id', 'name', 'type', 'based_on_version', 'based_on', 'notify'];

//    /**
//     * The relations to eager load on every query.
//     *
//     * @var array
//     */
//    protected $with = ['documentFiles'];

    /********** Override **********/
    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        if (Auth::user()->workspace && !isset($attributes['workspace_id'])) {
            $attributes['workspace_id'] = Auth::user()->workspace->id;
        }
        return parent::fill($attributes);
    }
    /********** Override **********/

    /********** Voyager relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function workspaceId()
    {
        return $this->belongsTo(\App\Workspace::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function categoryId()
    {
        return $this->belongsTo(\App\Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function basedOn()
    {
        return $this->hasOne(\App\Document::class, 'id', 'based_on');
    }
    /********** Voyager relations **********/

    /********** Relations **********/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function documentFiles()
    {
        return $this->hasMany(\App\DocumentFile::class);
    }
    public function documentfile()
    {
        return $this->documentFiles();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function signedDocumentFiles()
    {
        return $this->hasMany(\App\DocumentFile::class)->where('signed', '>', 0)->whereIn('type', config('document.signable'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function pendingDocumentFiles()
    {
        return $this->hasMany(\App\DocumentFile::class)->where('signed', null)->whereIn('type', config('document.signable'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function workspace()
    {
        return $this->workspaceId();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->categoryId();
    }
    /********** Relations **********/

    /********** Clone **********/
    public function clone() {
        $documentFile = $this->newest_document_file;

        $clone = $this->replicate();
        $clone->workspace_id = Auth::user()->workspace->id;
        $clone->based_on = $this->id;
        $clone->save();

        $cloneFile = $documentFile->replicate();
        $cloneFile->document_id = $clone->id;
        unset($cloneFile->signed, $cloneFile->signature);

        if ($documentFile->hasMedia('file')) {
            $cloneFile->copyMedia($documentFile->getFirstMedia('file')->getPath())->toMediaCollection('file');
        }

        $cloneFile->save();
    }
    /********** Clone **********/

    /********** Signable **********/
    public function getSignableAttribute($value = '')
    {
        return $this->generator->signable();
    }
    public function getNewestDocumentFileAttribute($value = '')
    {
        if (Auth::user()->hasPermission('edit_documents')) {
            return $this->documentFiles->sortByDesc('version')->first();
        }
        return $this->newest_signed_document_file;
    }
    public function getNewestSignedDocumentFileAttribute($value = '')
    {
        return $this->signable ? $this->documentFiles->sortByDesc('version')->filter->signed->first() : null;
    }
    /********** Signable **********/

    /********** Edit / Version / Pending Button **********/
    public function getEditVersionAttribute($value = '')
    {
        return optional($this->newest_document_file)->signed && $this->signable;
    }
    public function getEditVersionUrlAttribute($value = '')
    {
        if (request()->route()->named('sign.index')) {
            return route('sign.edit', $this->id);
        } elseif (request()->route()->named('notification.index')) {
            return route('notification.edit', $this->id);
        }
        return $this->edit_version ? route('version.edit', $this->id) : route('document.edit', $this->id);
    }
    public function getEditVersionIconAttribute($value = '')
    {
        return $this->edit_version ? 'fa fa-files-o' : 'fa fa-edit';
    }
    /********** Edit / Version / Pending Button **********/

    /********** View Button **********/
    public function getViewUrlAttribute($value = '')
    {
        return route('document.show', $this->id);
    }
    public function getViewIconAttribute($value = '')
    {
        return $this->generator->icon();
    }
    /********** View Button **********/

    /********** Generator **********/
    public function getGeneratorAttribute($value = '') {
        return class_exists($this->type) ? optional(new $this->type($this)) : optional(null);
    }
    /********** Generator **********/

    /********** Download Log **********/
    public function logged()
    {
        if (Auth::user()->hasPermission('edit_documents')) return;

        return $this->newest_document_file->downloadLogs()->create(['user_id' => Auth::user()->id]);
    }
    /********** Download Log **********/

    /********** Mutators **********/

    /********** Signable **********/
    public function setNotifyAttribute($value)
    {
        $this->attributes['notify'] = boolval($value);
    }
    /********** Mutators **********/
}
