<?php

namespace App\Generator;

class File extends Generator
{
    public function icon() {
        $icon = 'fa fa-file-photo-o';
        switch (optional($this->generate())->mime_type) {
            case 'application/pdf':
                $icon = 'fa fa-file-pdf-o';
                break;
            case 'application/vnd.ms-office':
                $icon = 'fa fa-file-excel-o';
                break;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            case 'application/msword':
                $icon = 'fa fa-file-word-o';
                break;
        }
        return $icon;
    }

    public function rules($action = '') {
        $rules = [
            'name' => 'required|string|max:255',
            'file' => 'required|max:10000',
        ];
        switch ($action) {
            case 'update':
                $rules = [
                    'name' => 'required|string|max:255'
                ];
                break;
        }
        return $rules;
    }

    public function generate()
    {
        return optional($this->document->getNewestDocumentFileAttribute())->getFirstMedia('file');
    }
}