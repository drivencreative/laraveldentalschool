<?php

namespace App\Generator;

use App\Document;

abstract class Generator
{
    /**
     * @var Document
     **/
    protected $document = null;

    /**
     * Generator constructor.
     * @param Document $document
     */
    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    public function signable() {
        return in_array($this->document->type, config('document.signable'));
    }

    public function icon() {
        return 'fa fa-file-pdf-o';
    }

    public function rules($action = '') {
        return [
            'name' => 'required|string|max:255',
            'content' => 'sometimes|required',
        ];
    }

    abstract public function generate();
}