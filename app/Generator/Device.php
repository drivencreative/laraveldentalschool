<?php

namespace App\Generator;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class Device extends Pdf
{
    /**
     * @param Collection $devices
     * @param int $category
     * @return \App\Document
     **/
    public static function create(Collection $devices = null, $category)
    {
        $devices = $devices ?: Auth::user()->workspace->devices()->where('category_id', $category)->get();
        $document = Auth::user()->workspace->documents()->firstOrNew(['type' => Device::class, 'category_id' => $category]);
        $category = \App\Category::find($category);

        $document->name = __('device.device') . ' - ' . $category->name;
        $document->type = Device::class;
        $document->save();

        $documentFile = $document->newest_document_file ?: new \App\DocumentFile;
        $documentFile->content = view('device.initialize.list', ['devices' => $devices, 'category' => $category]);
        $document->documentFiles()->save($documentFile);

        return $document;
    }
}