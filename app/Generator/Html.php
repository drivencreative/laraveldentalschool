<?php

namespace App\Generator;

use Illuminate\Support\Facades\Auth;

class Html extends Generator
{
    /**
     * @return mixed
     */
    public function generate()
    {
        $pdf = \PDF::loadView('generator.html', ['document' => $this->document]);

        $pdf->setOption('header-html', temporaryFile(view('generator.header'), 'header' . Auth::user()->workspace->id));
        $pdf->setOption('footer-html', temporaryFile(view('generator.footer')->withDocument($this->document), 'footer' . Auth::user()->workspace->id));

        return @$pdf->download(str_slug($this->document->name) . '.pdf');
    }
}