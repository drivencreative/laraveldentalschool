<?php

namespace App\Generator\Document;

use App\Generator\Pdf;

class Hygieneplan extends Pdf
{
    /**
     * @param \App\Workspace $workspace
     * @return \App\Document
     **/
    public static function create(\App\Workspace $workspace)
    {
        $document = $workspace->documents()->where('type', Hygieneplan::class)->first();
        if (!$document) {
            $document = new \App\Document;
            $document->name = config('document.hygieneplan.name') . $workspace->id;
            $document->type = Hygieneplan::class;
            $document->category_id = config('document.hygieneplan.category');
            $document->save();

            $documentFile = new\App\DocumentFile;
            $documentFile->content = view('generator.document.Hygieneplan.content');
            $documentFile->additional_content = view('generator.document.Hygieneplan.additional_content')->withWorkspace($workspace);

            $document->documentFiles()->save($documentFile);
        }

        return $document;
    }
}