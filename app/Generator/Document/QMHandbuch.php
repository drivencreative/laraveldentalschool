<?php

namespace App\Generator\Document;

use App\Generator\Pdf;
use iio\libmergepdf\Merger;

class QMHandbuch extends Pdf
{
    /**
     * @param \App\Workspace $workspace
     * @return \App\Document
     **/
    public static function create(\App\Workspace $workspace, \App\Document $organigramm = null)
    {
        $organigramm = $organigramm ?: \App\Generator\Document\Organigramm::create($workspace);

        if ($document = $workspace->documents()->where('type', QMHandbuch::class)->first()) {
            $documentFile = $document->newest_document_file ?: new \App\DocumentFile;
        } else {
            $document = new \App\Document;
            $documentFile = new \App\DocumentFile;
        }

        $document->name = config('document.qmhandbuch.name') . $workspace->id;
        $document->type = QMHandbuch::class;
        $document->category_id = config('document.qmhandbuch.category');
        $document->save();

        // Generate Cover page
        $pdf = \PDF::loadView('generator.document.QMHandbuch.content', ['workspace' => $workspace]);
        $pdf->setOrientation('landscape')->setOption('margin-top', 5)->setOption('margin-bottom', 5);
        @$pdf->save($QMHandbuch = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $document->name . 'pdf', true);

        // Combine PDF's
        $merger = new Merger;
        $merger->addFile($QMHandbuch); // Cover
        $merger->addFile($organigramm->newest_document_file->getFirstMedia('file')->getPath()); // Organigramm
        $merger->addFile(resource_path("views/generator/document/QMHandbuch/QM-Handbuch-Content.pdf")); // Content
        $filename = temporaryFile($merger->merge(), $document->name, '.pdf');

        $documentFile->clearMediaCollection('file');
        $documentFile->addMedia($filename)->toMediaCollection('file');
        $document->documentFiles()->save($documentFile);

        return $document;
    }
}