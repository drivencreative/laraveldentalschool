<?php

namespace App\Generator\Document;

use App\Generator\Pdf;

class Organigramm extends Pdf
{
    /**
     * @param \App\Workspace $workspace
     * @return \App\Document
     **/
    public static function create(\App\Workspace $workspace)
    {
        if ($document = $workspace->documents()->where('type', Organigramm::class)->first()) {
            $documentFile = $document->newest_document_file ?: new \App\DocumentFile;
        } else {
            $document = new \App\Document;
            $documentFile = new \App\DocumentFile;
        }

        $document->name = config('document.organigramm.name') . $workspace->id;
        $document->type = Organigramm::class;
        $document->category_id = config('document.organigramm.category');
        $document->save();

        $pdf = \PDF::loadView('generator.document.Organigramm.content', ['workspace' => $workspace]);
        $pdf->setOrientation('landscape')->setOption('margin-top', 5)->setOption('margin-bottom', 5);
        @$pdf->save($filename = storage_path("generator/{$document->name}.pdf"), true);

        $documentFile->clearMediaCollection('file');
        $documentFile->addMedia($filename)->toMediaCollection('file');
        $document->documentFiles()->save($documentFile);

        return $document;
    }
}