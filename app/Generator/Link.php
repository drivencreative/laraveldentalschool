<?php

namespace App\Generator;

class Link extends Generator
{
    public function icon() {
        return 'fa fa-globe';
    }

    public function generate()
    {
        return redirect(strip_tags($this->document->getNewestDocumentFileAttribute()->content));
    }
}