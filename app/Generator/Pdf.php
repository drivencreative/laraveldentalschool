<?php

namespace App\Generator;

class Pdf extends Generator
{
    public function generate()
    {
        $class = Html::class;
        if ($this->document->newest_document_file->hasMedia('file')) {
            $class = File::class;
        }

        return (new $class($this->document))->generate();
    }
}