<?php

namespace App;

class Role extends \TCG\Voyager\Models\Role
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name',
    ];

}
