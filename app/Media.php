<?php

namespace App;

class Media extends \Spatie\MediaLibrary\Media
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        return response()
            ->file($this->getPath(), [
                'Content-Type' => $this->mime_type,
                'Content-Disposition' => 'attachment; filename=' . $this->file_name,
            ]);
    }
}
