<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

### Checkout and Install
    git clone git@gitlab.artif.com:dentalschool/eqhm_laravel.git
    cd <Destination directory>
    php71 composer.phar install

### Database dump
    database/backup.sql

### EQHM
    http://dev02-04.dev.artif.net/

### Voyager - Admin Panel
    http://dev02-04.dev.artif.net/admin/

### Assets
    Documentation: https://laravel.com/docs/5.5/mix

    Css: resources/assets/sass
    JS: resources/assets/js

    npm install
    npm run production
    
### Controller & Templates
    Model: app/
    Controller: app/Http/Controllers/
    Template: resources/views/

    Middleware: app/Http/Middleware/WorkspaceContract.php

    Document:
        Config: config/document.php
        Model: app/Document.php
        Controller: app/Http/Controllers/DocumentController.php
        Template: resources/views/document

        Initialize:
            Controller: app/Http/Controllers/InitializeController.php
        Notification:
            Controller: app/Http/Controllers/NotificationController.php
            Template: resources/views/document/notification
        Sign:
            Controller: app/Http/Controllers/SignController.php
            Template: resources/views/document/sign
        Version:
            Controller: app/Http/Controllers/VersionController.php
            Template: resources/views/document/notification

    Workspace:
        Model: app/Workspace.php
        Controller: app/Http/Controllers/WorkspaceController.php
        Template: resources/views/workspace

    Device:
        Model: app/Device.php
        Controller: app/Http/Controllers/DeviceController.php
        Template: resources/views/device

        Initialize:
            Controller: app/Http/Controllers/InitializeController.php
            Template: resources/views/device/initialize

    PDF Generator:
        FPDI: https://github.com/hanneskod/libmergepdf
        WkHtmlToPdf: https://github.com/barryvdh/laravel-snappy
        Config: config/snappy.php
        Controller: app/Generator
        
        Static Document:
            Controller: app/Generator/Document