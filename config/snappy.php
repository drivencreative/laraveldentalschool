<?php

return array(

    'pdf' => array(
        'enabled' => true,
        'binary'  => base_path('vendor/bin/wkhtmltopdf-amd64'),
        'timeout' => false,
        'options' => array(
            'margin-top'        => 25,
            'margin-right'      => 5,
            'margin-bottom'     => 35,
            'margin-left'       => 5,
            'header-spacing'    => 2,
            'footer-spacing'    => 2,
        ),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => '/usr/local/bin/wkhtmltoimage',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),

);
