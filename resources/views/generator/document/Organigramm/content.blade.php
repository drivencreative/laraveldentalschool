<html>
<head>
    <meta charset="UTF-8">
    <title>Organigramm</title>

    <link rel="stylesheet" href="{{ asset('generator/pdf.css') }}">
</head>
<body>
<div class="organigramm">
    <div class="container">
        <div class="row">
            <div class="box w30">
                <span class="doc-name">Organigramm</span>
            </div>
            <div class="box w25 text-center border">
                <h3>Zahnarztpraxis</h3>
                <p>{{ $workspace->name }}</p>
            </div>
        </div>
        <div class="row">
            <div class="box w15 h200 text-center border">
                Verantwortlicher<br/>
                Zahnarzt (vZA)<br/>
                {{ $workspace->signature[0]->name }}
            </div>
            <div class="box w25 h200 border">
                Qualitätsmanagementbeauftragte(r) QMB: {{ $workspace->signature[1]->name }}*<br/>
                Medizinproduktebeauftragte(r); {{ $workspace->medical_officers }}*<br/>
                Sicherheitsbeauftragte(r); {{ $workspace->security_officers }}*<br/>
                Laserschutzbeauftragte(r); {{ $workspace->laser_protection }}*
            </div>
            <div class="box w15 h200 text-center border">
                Assistenzzahnärzte<br/>
                {{ $workspace->dental_assistant }}
            </div>
            <div class="box w12 h200 text-center border">
                Praxislabor<br/>
                {{ $workspace->practice_laboratory }}
            </div>
        </div>
        <div class="row">
            <div class="box w12 h100 text-center border">
                Hygieneverantwortliche(r)<br/>(HygV)<br/>
                {{ $workspace->signature[2]->name }}
            </div>
            <div class="box w11 h100 text-center border">
                Leitende ZFA<br/>
                {{ $workspace->leading_zfa }}
            </div>
            <div class="box w20 h100 text-center border">
                ZFAs<br/>
                {{ $workspace->zfas }}
            </div>
            <div class="box w11 h100 text-center border">
                Rezeption<br/>
                NamenRezeption<br/>
                {{ $workspace->reception }}
            </div>
            <div class="box w11 h100 text-center border">
                Abrechnung<br/>
                {{ $workspace->accounting }}
            </div>
        </div>
        <div class="row">
            <div class="box w15 h70 text-center border">
                Finanzbuchhaltung<br/>
                {{ $workspace->financial_accounting }}
            </div>
            <div class="box w15 h70 text-center border">
                Personal<br/>
                {{ $workspace->personal_representation }}
            </div>
            <div class="box w15 h70 text-center border">
                Einkauf<br/>
                {{ $workspace->purchase }}
            </div>
            <div class="box w15 h70 text-center border">
                IT-Organisation<br/>
                {{ $workspace->it_organization }}
            </div>
        </div>
    </div>
</div>
</body>
</html>