<div class="content-area">
    <h1 class="title">
        <b>Hygieneplan</b>
    </h1>
    <div class="content">{{ $workspace->name }}</div>
    <div class="content">Stand: {{ $workspace->updated_at->format('d. m. Y') }}</div>
    <div class="content">Dieser Hygieneplan wurde nach der Vorlage des DAHZ/BZÄK-Musterhygieneplans
        (Stand <span class="date">{{ $workspace->updated_at->format('d. m. Y') }}</span>) erstellt.<br/><br/>

        Der vorliegende Hygieneplan ist eine Dienstanweisung und muss von allen Mitarbeitern befolgt werden. Ziel des
        Hygieneplanes ist es, sowohl die Patienten als auch das Personal vor Infektionen zu schützen.<br/><br/>

        Die Unterweisung erfolgt in regelmäßigen Schulungen, die vom Praxisinhaber und der Hygieneverantwortlichen
        (HygV) durchgeführt werden. Über diese Schulung wird eine separate Aufzeichnung geführt in der jeder Mitarbeiter
        mit seiner Unterschrift bestätigt, dass er über den Hygieneplan unterwiesen wurde.<br/><br/>

        Die Aktualisierungen werden regelmäßig vorgenommen und als Revisionsstände dokumentiert. Dieser Hygieneplan ist
        gültig ab <span class="date">{{ $workspace->updated_at->format('d. m. Y') }}</span> bis zum Erscheinen eines neuen Hygieneplanes, jeweils in der aktuellsten Fassung<br/><br/>

    </div>
</div>
<div class="page-break"></div>