<html>
<head>
    <meta charset="UTF-8">
    <title>QMHandbuch</title>

    <link rel="stylesheet" href="{{ asset('generator/pdf.css') }}">
</head>
<body>
    <div class="QMHandbuch">
        <div class="container">
            <div class="row">
                <div class="box w100 text-center">
                    <h1 class="doc-name">QM- Handbuch</h1>
                    <h4>der</h4>
                    <h2 class="practice-name">Zahnärztlichen Praxis</h2>
                    <h2 class="practice-name">{{ $workspace->name }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="box w47">
                    Stand: {{ $workspace->updated_at->format('d. m. Y') }}
                </div>
                <div class="box w47 align-right">
                    verantwortlicher Zahnarzt vZA:<br/>
                    @if($workspace->signature[0]->hasMedia('file'))
                        <img src="{{ $workspace->signature[0]->getFirstMedia('file')->getUrl() }}" width="100"/>
                    @else
                        {{ $workspace->signature[0]->name }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</body>
</html>