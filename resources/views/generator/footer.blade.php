<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="{{ asset('generator/pdf.css') }}">
</head>
<body onload="substitutePdfVariables()">
    <div class="footer">
        <span></span>
        <table>
            <tbody>
                @if ($document->newest_document_file->signed)
                    <tr>
                        <td>{{ __('document.release_date') }}</td>
                        <td>{{ __("signature.{$document->newest_document_file->signature_index}.name") }}</td>
                    </tr>
                    <tr>
                        <td>{{ $document->newest_document_file->signed->format('d. m. Y') }}</td>
                        <td>{{ $document->newest_document_file->signature }}</td>
                        <td rowspan="2">
                            @if ($document->newest_document_file->hasMedia('signature'))
                                <img src="{{ asset($document->newest_document_file->getFirstMedia('signature')->getUrl()) }}" style="max-width: 100px; max-height: 40px"/>
                            @endif
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="text-align: right;" colspan="3"><span class="page"></span> / <span class="topage"></span></td>
                </tr>
            </tbody>
        </table>
    </div>
    <script>
        function substitutePdfVariables() {
            function getParameterByName(name) {
                var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }
            function substitute(name) {
                var value = getParameterByName(name);
                var elements = document.getElementsByClassName(name);
                for (var i = 0; elements && i < elements.length; i++) {
                    elements[i].textContent = value;
                }
            }
            ['frompage', 'topage', 'page', 'webpage', 'section', 'subsection', 'subsubsection'].forEach(function(param) { substitute(param); });
        }
    </script>
</body>
</html>