<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="{{ asset('generator/pdf.css') }}">
</head>
<body>
    @if($document->newest_document_file->additional_content)
        <div class="container">
            {!! $document->newest_document_file->additional_content !!}
        </div>
    @endif
    <div class="container">
        {!! $document->newest_document_file->content !!}
    </div>
</body>
</html>