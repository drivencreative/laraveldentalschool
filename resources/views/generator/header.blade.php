<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="{{ asset('generator/pdf.css') }}">
</head>
<body>
    @include('document.partial.header')
</body>
</html>