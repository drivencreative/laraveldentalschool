@extends('layouts.app')

@section('content')
    @php
        if (!old()) {
            request()->session()->flashInput([
                'name' => $document->name,
                'content' => optional($document->newest_document_file)->content,
                'additional_content' => optional($document->newest_document_file)->additional_content
            ]);
        }
    @endphp
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.version_document') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('version.update', $document->id) }}" enctype="multipart/form-data">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            @include('partials.textfield', ['field' => 'name', 'label' => __('document.name'), 'required' => true])

                            @switch($document->type)
                                @case(\App\Generator\File::class)
                                    {{--@include('partials.file', ['field' => 'file', 'label' => __('document.file'), 'required' => true])--}}
                                @break
                                @case(\App\Generator\Link::class)
                                    @include('partials.textfield', ['field' => 'content', 'label' => __('document.link'), 'required' => true])
                                @break
                                @default
                                    @if(old('additional_content'))
                                        @include('partials.richtext', ['field' => 'additional_content', 'label' => __('document.additional_content'), 'required' => true, 'attributes' => 'readonly="readonly'])
                                    @endif
                                    @include('partials.richtext', ['field' => 'content', 'label' => __('document.content'), 'required' => true])
                            @endswitch

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
