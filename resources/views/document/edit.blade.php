@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.edit_document') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('document.update', $document->id) }}" enctype="multipart/form-data">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            @include('partials.textfield', ['field' => 'name', 'label' => __('document.name'), 'required' => true, 'value' => $document->name])

                            @switch($document->type)
                                @case(\App\Generator\File::class)
                                @case(\App\Generator\Document\Organigramm::class)
                                @case(\App\Generator\Document\QMHandbuch::class)
                                    @include('document.show.file')
                                @break
                                @case(\App\Generator\Link::class)
                                    @include('partials.textfield', ['field' => 'content', 'label' => __('document.link'), 'required' => true, 'value' => optional($document->newest_document_file)->content])
                                @break
                                @default
                                    @include('partials.richtext', ['field' => 'content', 'label' => __('document.content'), 'required' => true, 'value' => optional($document->newest_document_file)->content, 'attributes' => 'readonly="readonly"'])
                            @endswitch

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
