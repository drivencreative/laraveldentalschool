<div class="header">
    <span>{{ Auth::user()->workspace->name }} </span>
    <span>{{ implode(', ', array_filter(Auth::user()->workspace->only('street', 'zip', 'city', 'country'))) }}</span>
</div>