<div class="footer">
    <span></span>
    <table>
        <tbody>
        <tr>
            <td>{{ __('document.release_date') }}</td>
            <td>{{ __("signature.{$document->newest_document_file->signature_index}.name") }}</td>
        </tr>
        <tr>
            <td>{{ $document->newest_document_file->signed }}</td>
            <td>{{ $document->newest_document_file->signature }}</td>
            <td rowspan="2">
                @if($document->newest_document_file->hasMedia('signature'))
                    <img src="{{ $document->newest_document_file->getFirstMediaUrl('signature') }}" width="100px"/>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
</div>