@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.sign_document') }}</div>
                    <div class="panel-body">
                        <h3>{{ $document->name }}</h3>
                        <hr>
                        <form class="form-horizontal" method="POST" action="{{ route('sign.update', $document->id) }}" enctype="multipart/form-data">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            @switch($document->type)
                                @case(\App\Generator\File::class)
                                    @include('document.show.file')
                                @break
                                @case(\App\Generator\Link::class)
                                    @include('document.show.link')
                                @break
                                @default
                                    @include('document.show.html')
                            @endswitch

                            @include('partials.selectfield', ['field' => 'sign', 'label' => __('document.signature'), 'required' => true, 'value' => '', 'options' => Auth::user()->workspace->signature->pluck('name', 'id')->prepend(__('registration.please_select'), 0)])

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
