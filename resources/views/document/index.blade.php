@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            @if (request()->route()->named('document.index'))
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.categories') }}</div>
                    <div class="panel-body">
                        @include('document.categories')
                    </div>
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('document.links') }}</div>
                <div class="panel-body">
                    <div class="list-group-root">
                        <a href="{{ route('document.index') }}" class="list-group-item @if (request()->route()->named('document.index')) active @endif">
                            {{ __('document.document') }}
                        </a>
                        @if(Auth::user()->hasPermission('edit_documents'))
                            <a href="{{ route('sign.index') }}" class="list-group-item @if(request()->route()->named('sign.index')) active @endif">
                                {{ __('document.pending_documents') }}
                            </a>
                            <a href="{{ route('notification.index') }}" class="list-group-item @if(request()->route()->named('notification.index')) active @endif">
                                {{ __('document.notification_documents') }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('document.document') }}</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (request()->category && Auth::user()->hasPermission('edit_documents'))
                        <div class="btn-group btn-group-justified">
                            <a href="{{ route('document.create', ['type' => \App\Generator\Html::class, 'category' => request()->category]) }}" class="btn btn-default">{{ __('document.new.html') }}</a>
                            <a href="{{ route('document.create', ['type' => \App\Generator\File::class, 'category' => request()->category]) }}" class="btn btn-default">{{ __('document.new.file') }}</a>
                            <a href="{{ route('document.create', ['type' => \App\Generator\Link::class, 'category' => request()->category]) }}" class="btn btn-default">{{ __('document.new.link') }}</a>
                        </div>
                        <hr>
                    @endif

                    @if ($documents->count())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>{{ __('document.name') }}</td>
                                        <td>{{ __('document.version') }}</td>
                                        @if(Auth::user()->hasPermission('edit_documents'))
                                            <td></td>
                                        @endif
                                        @if (request()->route()->named('document.index'))
                                            <td></td>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($documents as $document)
                                        <tr>
                                            <td>{{ $document->name }}</td>
                                            <td>{{ optional($document->newest_document_file)->version }}</td>
                                            @if(Auth::user()->hasPermission('edit_documents'))
                                                <td align="center" style="font-size: 2rem;">
                                                    <a href="{{ $document->edit_version_url }}">
                                                        <span class="{{ $document->edit_version_icon }}"></span>
                                                    </a>
                                                </td>
                                            @endif
                                            @if (request()->route()->named('document.index'))
                                                <td align="center" style="font-size: 2rem;">
                                                    <a href="{{ $document->view_url }}" class="{{ $document->newest_document_file->is_new ? 'text-danger' : '' }}">
                                                        <span class="{{ $document->view_icon }}"></span>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $documents->appends(request()->input())->links() }}
                    @else
                        <p>{{ __('document.no_documents_category') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
