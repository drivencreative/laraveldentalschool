@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.new_document') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('document.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <input type="hidden" name="type" value="{{ request()->type }}">
                            <input type="hidden" name="category_id" value="{{ request()->category }}">

                            @include('partials.textfield', ['field' => 'name', 'label' => __('document.name'), 'required' => true])

                            @switch(request()->type)
                                @case(\App\Generator\Html::class)
                                    @include('partials.richtext', ['field' => 'content', 'label' => __('document.content'), 'required' => true])
                                @break
                                @case(\App\Generator\File::class)
                                    @include('partials.file', ['field' => 'file', 'label' => __('document.file'), 'required' => true])
                                @break
                                @case(\App\Generator\Link::class)
                                    @include('partials.textfield', ['field' => 'content', 'label' => __('document.link'), 'required' => true])
                                @break
                                @default
                                    @include('partials.textarea', ['field' => 'content', 'label' => __('document.content'), 'required' => true])
                            @endswitch

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
