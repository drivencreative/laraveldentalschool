<div>
    <p>{{ isset($label) ? $label : __('document.preview') }}</p>
    @if($document->newest_document_file->hasMedia('file'))
        <hr>
        @switch($document->newest_document_file->getFirstMedia('file')->mime_type)
            @case('application/pdf')
                <embed src="{{ $document->newest_document_file->getFirstMedia('file')->getUrl() }}" width="100%" height="600"  />
            @break
            @default
                <img src="{{ $document->newest_document_file->getFirstMedia('file')->getUrl() }}" />
        @endswitch
        <hr>
    @endif
</div>