@extends('layouts.app')

@section('content')
    @php
        if (!old()) {
            request()->session()->flashInput([
                'name' => $document->name,
                'content' => optional($document->newest_document_file)->content,
                'additional_content' => optional($document->newest_document_file)->additional_content
            ]);
        }
    @endphp
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.version_document') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('notification.update', $document->id) }}" enctype="multipart/form-data">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            <div class="panel panel-default">
                                <div class="panel-heading">{{ __('document.name') }}</div>
                                <div class="panel-body">
                                    @if($document->based_on)
                                        @include('partials.textfield', ['field' => 'based_on[name]', 'label' => __('document.original_version'), 'required' => false, 'value' => $document->basedOn->name, 'attributes' => 'readonly="readonly"'])
                                    @endif
                                    @include('partials.textfield', ['field' => 'name', 'label' => __('document.your_version'), 'required' => true])
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">{{ __('document.content') }}</div>
                                <div class="panel-body">
                                    @switch($document->type)
                                        @case(\App\Generator\File::class)
                                        @case(\App\Generator\Document\Organigramm::class)
                                        @case(\App\Generator\Document\QMHandbuch::class)
                                            @if($document->based_on)
                                                @include('document.show.file', ['document' => $document->basedOn, 'label' => __('document.original_version')])
                                            @endif
                                            @include('document.show.file', ['document' => $document, 'label' => __('document.your_version')])
                                        @break
                                        @case(\App\Generator\Link::class)
                                            @if($document->based_on)
                                                @include('partials.textfield', ['field' => 'based_on[content]', 'label' => __('document.original_version'), 'required' => false, 'value' => optional($document->basedOn->newest_document_file)->content, 'attributes' => 'readonly="readonly"'])
                                            @endif
                                            @include('partials.textfield', ['field' => 'content', 'label' => __('document.your_version'), 'required' => true])
                                        @break
                                        @default
                                            @if($document->based_on)
                                                @include('partials.richtext', ['field' => 'based_on[content]', 'label' => __('document.original_version'), 'required' => false, 'value' => optional($document->basedOn->newest_document_file)->content, 'attributes' => 'readonly="readonly"'])
                                            @endif
                                            @include('partials.richtext', ['field' => 'content', 'label' => __('document.your_version'), 'required' => true])
                                    @endswitch
                                </div>
                            </div>

                            <div class="text-center">
                                <input value="{{ __('document.compare_version.apply') }}" name="clone" type="submit" class="btn btn-primary">
                                <input value="{{ __('document.compare_version.keep') }}" type="submit" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
