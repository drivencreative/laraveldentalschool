@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        @include('partials.textfield', ['field' => 'name', 'label' => __('registration.name'), 'required' => true])

                        @include('partials.emailfield', ['field' => 'email', 'label' => __('registration.email'), 'required' => true])

                        @include('partials.passwordfield', ['field' => 'password', 'label' => __('registration.password'), 'required' => true])

                        @include('partials.passwordfield', ['field' => 'password_confirmation', 'label' => __('registration.password_confirmation'), 'required' => true])

                        @include('partials.submitfield', ['label' => __('registration.submit')])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
