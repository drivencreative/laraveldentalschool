@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6">
                            <h2>{{ __('order.header.billing_address') }}</h2>
                            <p>{{ $user->name }} {{ $user->surname }}</p>
                            <p>{{ $user->street }}</p>
                            <p>{{ $user->city }}</p>
                            <p>{{ __('registration.lands.' . $user->land) }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6 checkbox">
                            <label data-toggle="collapse" data-target="#billing_address" aria-expanded="false" aria-controls="billing_address">
                                <input type="checkbox" name="different_billing_address" value="1" {{ old('different_billing_address') ? 'checked="checked"' : '' }} />
                                {{ __('order.company_address') }}
                            </label>
                        </div>
                    </div>
                    <div id="billing_address" aria-expanded="false" class="{{ old('different_billing_address') ? 'collapse in' : 'collapse' }}">
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.personal') }}</h4>
                            </div>
                        </div>

                        @include('partials.selectfield', ['field' => 'gender', 'label' => __('registration.gender'), 'required' => true, 'options' => [
                            '' => __('registration.please_select'),
                            1 => __('registration.genders.1'),
                            2 => __('registration.genders.2')
                        ]])

                        @include('partials.textfield', ['field' => 'name', 'label' => __('registration.name'), 'required' => true])

                        @include('partials.textfield', ['field' => 'surname', 'label' => __('registration.surname'), 'required' => true])

                        @include('partials.emailfield', ['field' => 'email', 'label' => __('registration.email'), 'required' => true])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.company') }}</h4>
                            </div>
                        </div>

                        @include('partials.textfield', ['field' => 'company', 'label' => __('registration.company'), 'required' => false])

                        @include('partials.textfield', ['field' => 'company2', 'label' => __('registration.company2'), 'required' => false])

                        @include('partials.textfield', ['field' => 'company3', 'label' => __('registration.company3'), 'required' => false])

                        @include('partials.textfield', ['field' => 'ust_id', 'label' => __('registration.ust_id'), 'required' => false])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.address') }}</h4>
                            </div>
                        </div>

                        @include('partials.textfield', ['field' => 'street', 'label' => __('registration.street'), 'required' => true])

                        @include('partials.textfield', ['field' => 'zip', 'label' => __('registration.zip'), 'required' => true])

                        @include('partials.textfield', ['field' => 'city', 'label' => __('registration.city'), 'required' => true])

                        @include('partials.selectfield', ['field' => 'land', 'label' => __('registration.land'), 'required' => true, 'options' => [
                            '' => __('registration.please_select'),
                            1 => __('registration.lands.1'),
                            2 => __('registration.lands.2')
                        ]])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.contact') }}</h4>
                            </div>
                        </div>

                        @include('partials.textfield', ['field' => 'phone', 'label' => __('registration.phone'), 'required' => false])

                        @include('partials.textfield', ['field' => 'fax', 'label' => __('registration.fax'), 'required' => false])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.password') }}</h4>
                                <p class="text-left">{{ __('registration.header.password_text') }}</p>
                            </div>
                        </div>

                        @include('partials.passwordfield', ['field' => 'password', 'label' => __('registration.password'), 'required' => true])

                        @include('partials.passwordfield', ['field' => 'password_confirmation', 'label' => __('registration.password_confirmation'), 'required' => true])

                        @include('partials.submitfield', ['label' => __('registration.submit')])
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6">
                            <h2>{{ __('order.header.delivery_address') }}</h2>
                            <p>{{ $user->address->name }} {{ $user->address->surname }}</p>
                            <p>{{ $user->address->street }}</p>
                            <p>{{ $user->address->city }}</p>
                            <p>{{ __('registration.lands.' . $user->address->land) }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6 checkbox">
                            <label data-toggle="collapse" data-target="#delivery_address" aria-expanded="false" aria-controls="delivery_address">
                                <input type="checkbox" name="different_delivery_address" value="1" {{ old('different_delivery_address') ? 'checked="checked"' : '' }} />
                                {{ __('order.delivery_address') }}
                            </label>
                        </div>
                    </div>
                    <div id="delivery_address" aria-expanded="false" class="{{ old('different_delivery_address') ? 'collapse in' : 'collapse' }}">
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.personal') }}</h4>
                            </div>
                        </div>

                        @include('partials.selectfield', ['field' => 'address[gender]', 'label' => __('registration.gender'), 'required' => true, 'options' => [
                            '' => __('registration.please_select'),
                            1 => __('registration.genders.1'),
                            2 => __('registration.genders.2')
                        ]])

                        @include('partials.textfield', ['field' => 'address[name]', 'label' => __('registration.name'), 'required' => true])

                        @include('partials.textfield', ['field' => 'address[surname]', 'label' => __('registration.surname'), 'required' => true])

                        @include('partials.emailfield', ['field' => 'address[email]', 'label' => __('registration.email'), 'required' => true])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.company') }}</h4>
                            </div>
                        </div>

                        @include('partials.textfield', ['field' => 'address[company]', 'label' => __('registration.company'), 'required' => false])

                        @include('partials.textfield', ['field' => 'address[company2]', 'label' => __('registration.company2'), 'required' => false])

                        @include('partials.textfield', ['field' => 'address[company3]', 'label' => __('registration.company3'), 'required' => false])

                        @include('partials.textfield', ['field' => 'address[ust_id]', 'label' => __('registration.ust_id'), 'required' => false])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.address') }}</h4>
                            </div>
                        </div>

                        @include('partials.textfield', ['field' => 'address[street]', 'label' => __('registration.street'), 'required' => true])

                        @include('partials.textfield', ['field' => 'address[zip]', 'label' => __('registration.zip'), 'required' => true])

                        @include('partials.textfield', ['field' => 'address[city]', 'label' => __('registration.city'), 'required' => true])

                        @include('partials.selectfield', ['field' => 'address[land]', 'label' => __('registration.land'), 'required' => true, 'options' => [
                            '' => __('registration.please_select'),
                            1 => __('registration.lands.1'),
                            2 => __('registration.lands.2')
                        ]])

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-6 control-label">
                                <h4 class="text-left">{{ __('registration.header.contact') }}</h4>
                            </div>
                        </div>

                        @include('partials.textfield', ['field' => 'address[phone]', 'label' => __('registration.phone'), 'required' => false])

                        @include('partials.textfield', ['field' => 'address[fax]', 'label' => __('registration.fax'), 'required' => false])

                        @include('partials.submitfield', ['label' => __('registration.submit')])
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
