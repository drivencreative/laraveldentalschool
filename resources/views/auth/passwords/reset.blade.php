@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        @include('partials.emailfield', ['field' => 'email', 'label' => __('registration.email'), 'required' => true])

                        @include('partials.passwordfield', ['field' => 'password', 'label' => __('registration.password'), 'required' => true])

                        @include('partials.passwordfield', ['field' => 'password_confirmation', 'label' => __('registration.password_confirmation'), 'required' => true])

                        @include('partials.submitfield', ['label' => __('login.reset_password')])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
