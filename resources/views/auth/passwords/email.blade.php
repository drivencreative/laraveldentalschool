@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        @include('partials.emailfield', ['field' => 'email', 'label' => __('registration.email'), 'required' => true])

                        @include('partials.submitfield', ['label' => __('login.send_password_reset')])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
