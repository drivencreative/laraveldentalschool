@php $index = 0; @endphp

<label for="name">{{ $row->display_name }}</label>
@if($dataTypeContents)
    @foreach($dataTypeContents as $index => $dataTypeContent)
        @include('voyager::formfields.add-edit-item')
    @endforeach
@endif

<div>
    <a href="" class="btn btn-default" onclick="
            $(this).addClass('hidden');
            $(this).next().removeClass('hidden');
            $('#new-relation-{{ str_slug($row->relation ?: $row->field) }}-{{ $parentDataType->id }}-{{ $parentDataTypeContent->id }}').appendTo($(this).parent());
            $(this).parent().find('#new-relation-{{ str_slug($row->relation ?: $row->field) }}-{{ $parentDataType->id }}-{{ $parentDataTypeContent->id }}').find('textarea').each(function(index) {
            $(this).prev().remove();
            $(this).replaceWith($('<textarea>', {'name': $(this).attr('name'), 'class': 'form-control new-relation-rte-{{ str_slug($row->relation ?: $row->field) }}-{{ $parentDataType->id }}-{{ $parentDataTypeContent->id }}'}));
            tinymce.init({
                menubar: false,
                selector: 'textarea.new-relation-rte-{{ str_slug($row->relation ?: $row->field) }}-{{ $parentDataType->id }}-{{ $parentDataTypeContent->id }}',
                skin: 'voyager',
                min_height: 600,
                resize: 'vertical',
                plugins: 'link, image, code, youtube, giphy, table, textcolor, lists',
                extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
                file_browser_callback: function (e, t, n, i) {
                    return 'image' == n && $('#upload_file').trigger('click')
                },
                toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | code',
                convert_urls: !1,
                image_caption: !0,
                image_title: !0
            });
            });
            return false;">
        <i class="voyager-plus"></i>
    </a>
    <a href="" class="btn btn-default hidden" onclick="
            $(this).addClass('hidden');
            $(this).prev().removeClass('hidden');
            $(this).next().appendTo($('#modals'));
            return false;">
        <i class="voyager-trash"></i>
    </a>
    @section('modals')
        @parent
        <div id="new-relation-{{ str_slug($row->relation ?: $row->field) }}-{{ $parentDataType->id }}-{{ $parentDataTypeContent->id }}">
            @include('voyager::formfields.add-edit-item', ['dataTypeContent' => \Voyager::model('DataType'), 'index' => ++$index, 'active' => true])
        </div>
    @stop
</div>