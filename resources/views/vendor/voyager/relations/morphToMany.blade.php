<label for="name">{{ $row->display_name }}</label>
@php
    $options->options = app($dataType->model_name)->pluck($options->label, $options->key)->all();
    $row->relation = $row->field = $dataType->name;
    if (isset($relation)) {
        $row->relation = "{$relation}[{$dataType->name}]";
    }
    if ($dataTypeContent->{$row->field}) {
        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field}->pluck('id')->all();
    }
@endphp
@include('voyager::formfields.select_multiple')
