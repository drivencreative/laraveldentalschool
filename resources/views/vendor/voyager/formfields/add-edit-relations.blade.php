@php
    $options = json_decode($row->details);
    $parentDataType = $dataTypeContent;
    $parentDataTypeContent = $dataTypeContent;

    $dataType = \Voyager::model('DataType')->where('model_name', $options->model)->first();
    $dataTypeContents = null;
    if (isset($dataTypeContent->{$options->table})) {
        $dataTypeContents = $dataTypeContent->{$options->table};
    }
    if (is_a($dataTypeContents, \Illuminate\Database\Eloquent\Model::class)) {
        $dataTypeContents = [$dataTypeContents];
    }
    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )}->where('field', '!=', ($options->column != 'id' ? $options->column : ''));
@endphp

@includeFirst(["voyager::relations.{$row->field}", "voyager::relations.{$options->table}", "voyager::relations.{$options->type}", "voyager::relations.inline"])