@php
    // prevent ovewriting inside the relationship loop
    if (!isset($dontOverwriteRelation)) {
        $relation = '';
    }
@endphp
<div class="panel-group relation" data-name="{{ $dataType->name }}">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#relation-{{ $dataType->name }}-{{ $dataType->id }}-{{ $dataTypeContent->id }}">
                    {{ implode(', ', array_filter($dataTypeContent->only($dataTypeRows->filter->browse->pluck('field')->all()))) }}
                </a>
                @can('delete', $dataTypeContent)
                    <a href="" onclick="$('#delete-{{ $dataType->name }}-{{ $dataTypeContent->id }}').modal(); return false;" title="{{ __('voyager.generic.delete') }}" class="pull-right delete" data-id="{{ $dataTypeContent->id }}">
                        <i class="voyager-trash"></i>
                    </a>
                    @section('modals')
                        @parent
                        <div class="modal modal-danger fade" tabindex="-1" id="delete-{{ $dataType->name }}-{{ $dataTypeContent->id }}" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager.generic.close') }}"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager.generic.delete_question') }}?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{ route('voyager.'.$dataType->slug.'.destroy', $dataTypeContent->id) }}" method="POST">
                                            {{ method_field("DELETE") }}
                                            {{ csrf_field() }}
                                            <button type="submit" name="status" value="expired" class="btn btn-danger pull-right delete-confirm">
                                                {{ __('voyager.generic.delete_confirm') }}
                                            </button>
                                        </form>
                                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager.generic.cancel') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @stop
                @endcan
            </h4>
        </div>
        <div id="relation-{{ $dataType->name }}-{{ $dataType->id }}-{{ $dataTypeContent->id }}" class="panel-collapse collapse {{ isset($active) ? 'in' : '' }}">
            <div class="panel-body">
                @foreach($dataTypeRows as $row)
                    @php
                        // field name when multiple levels of relationships
                        if ($relation) {
                            $row->relation = "{$relation}[{$dataType->name}][{$index}][{$row->field}]";
                        } else {
                            $row->relation = "{$dataType->name}[{$index}][{$row->field}]";
                        }
                    @endphp
                    @if($row->type == 'relationship')
                        @php
                            // concatenating previous level of relationship
                            if ($relation) {
                                if (!str_contains($relation, ["{$dataType->name}[{$index}]", "[{$dataType->name}][{$index}]"])) {
                                    $relation .= "[{$dataType->name}][{$index}]";
                                }
                            } else {
                                $relation = "{$dataType->name}[{$index}]";
                            }
                        @endphp
                        @include('voyager::formfields.add-edit-relations', ['row' => $row, 'relation' => $relation, 'dontOverwriteRelation' => true])
                    @else
                        @include('voyager::formfields.add-edit')
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>