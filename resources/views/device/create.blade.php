@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('device.new_device') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('device.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <input type="hidden" name="category_id" value="{{ request()->category }}">

                            @include('partials.textfield', ['field' => 'type', 'label' => __('device.type'), 'required' => true])
                            @include('partials.textfield', ['field' => 'name', 'label' => __('device.name'), 'required' => true])
                            @include('partials.textfield', ['field' => 'manufacturer', 'label' => __('device.manufacturer'), 'required' => true])
                            @include('partials.textfield', ['field' => 'ce_mark', 'label' => __('device.ce_mark'), 'required' => false])
                            @include('partials.textfield', ['field' => 'serial_number', 'label' => __('device.serial_number'), 'required' => false])
                            @include('partials.textfield', ['field' => 'year_of_construction', 'label' => __('device.year_of_construction'), 'required' => false])
                            @include('partials.textfield', ['field' => 'space', 'label' => __('device.space'), 'required' => false])
                            @include('partials.textfield', ['field' => 'number', 'label' => __('device.number'), 'required' => false])

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
