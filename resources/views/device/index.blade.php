@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            @if (request()->route()->named('device.index'))
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.categories') }}</div>
                    <div class="panel-body">
                        @include('device.categories')
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('device.device') }}</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (request()->category && Auth::user()->hasPermission('edit_documents'))
                        <div class="btn-group">
                            <a href="{{ route('device.create', ['type' => \App\Generator\Html::class, 'category' => request()->category]) }}" class="btn btn-default">{{ __('device.new') }}</a>
                        </div>
                        <hr>
                    @endif

                    @if ($devices->count())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>{{ __('device.type') }}</td>
                                        <td>{{ __('device.name') }}</td>
                                        <td>{{ __('device.number') }}</td>
                                        @if(Auth::user()->hasPermission('edit_documents'))
                                            <td></td>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($devices as $device)
                                        <tr>
                                            <td>{{ $device->type }}</td>
                                            <td>{{ $device->name }}</td>
                                            <td>{{ $device->number }}</td>
                                            @if(Auth::user()->hasPermission('edit_documents'))
                                                <td align="center" style="font-size: 2rem;">
                                                    <a href="{{ route('device.edit', $device->id) }}">
                                                        <span class="fa fa-edit"></span>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $devices->appends(request()->input())->links() }}
                    @else
                        <p>{{ __('device.no_devices_category') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
