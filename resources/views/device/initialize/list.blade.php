@if($devices)
    <h2>{{ $category->name }}</h2>
    <table border="1" cellpadding="2" cellspacing="0" class="tx_artifeqhm">
        <tr>
            <th valign="top" width="120">
                {{ __('device.type') }}
            </th>
            <th valign="top">
                {{ __('device.name') }}
            </th>
            <th valign="top">
                {{ __('device.manufacturer') }}
            </th>
            <th valign="top">
                {{ __('device.number') }}
            </th>
            <th valign="top">
                {{ __('device.ce_mark') }}
            </th>
            <th valign="top">
                {{ __('device.serial_number') }}
            </th>
            <th valign="top">
                {{ __('device.year_of_construction') }}
            </th>
            <th valign="top">
                {{ __('device.space') }}
            </th>
            <th valign="top">
                {{ __('device.notes') }}
            </th>
        </tr>
        @foreach($devices as $device)
            <tr>
                <td>{{ $device->type }}</td>
                <td>{{ $device->name }}</td>
                <td>{{ $device->manufacturer }}</td>
                <td>{{ $device->number }}</td>
                <td>{{ $device->ce_mark }}</td>
                <td>{{ $device->serial_number }}</td>
                <td>{{ $device->year_of_construction }}</td>
                <td>{{ $device->space }}</td>
                <td></td>
            </tr>
        @endforeach
    </table>
@endif