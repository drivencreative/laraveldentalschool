@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('device.edit_device', ['category' => $device->category->name]) }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('device.update', $device->id) }}" enctype="multipart/form-data">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            @include('partials.textfield', ['field' => 'type', 'label' => __('device.type'), 'value' => $device->type, 'required' => true])
                            @include('partials.textfield', ['field' => 'name', 'label' => __('device.name'), 'value' => $device->name, 'required' => true])
                            @include('partials.textfield', ['field' => 'manufacturer', 'label' => __('device.manufacturer'), 'value' => $device->manufacturer, 'required' => true])
                            @include('partials.textfield', ['field' => 'ce_mark', 'label' => __('device.ce_mark'), 'value' => $device->ce_mark, 'required' => false])
                            @include('partials.textfield', ['field' => 'serial_number', 'label' => __('device.serial_number'), 'value' => $device->serial_number, 'required' => false])
                            @include('partials.textfield', ['field' => 'year_of_construction', 'label' => __('device.year_of_construction'), 'value' => $device->year_of_construction, 'required' => false])
                            @include('partials.textfield', ['field' => 'space', 'label' => __('device.space'), 'value' => $device->space, 'required' => false])
                            @include('partials.textfield', ['field' => 'number', 'label' => __('device.number'), 'value' => $device->number, 'required' => false])

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
