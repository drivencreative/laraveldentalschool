@if(!isset($categories))
    @php
        $categories = \App\Category::where('parent_id', null)->get();
        $class = 'list-group-root';
    @endphp
@endif

<div class="{{ $class }}" @if (isset($id)) id="item-{{ $id }}" @endif>
    @foreach($categories as $category)
        @if ($category->subCategories->count())
            @php
                $subCategory = in_array(request()->category, $category->subCategoriesIds());
            @endphp
            <span class="list-group-item @if ($category->id == request()->category) active @endif">
                <a href="#item-{{ $category->id }}" data-toggle="collapse" class="{{ $subCategory ? '' : 'collapsed' }}">
                    <i href="#item-{{ $category->id }}" class="glyphicon glyphicon-plus"></i>
                </a>
                <a href="{{ route('device.index', ['category' => $category->id]) }}" style="width: 90%;">
                    {{ $category->name }}
                </a>
                @include('device.categories', ['categories' => $category->subCategories, 'id' => $category->id, 'class' => 'collapse' . ($subCategory ? ' in' : '')])
            </span>
        @else
            <a href="{{ route('device.index', ['category' => $category->id]) }}" class="list-group-item @if ($category->id == request()->category) active @endif">
                {{ $category->name }}
            </a>
        @endif
    @endforeach
</div>