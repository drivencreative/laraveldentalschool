@extends('layouts.app')

@section('content')
    @php
        if (!old()) request()->session()->flashInput($workspace->toArray());
    @endphp

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('document.edit_document') }}</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-danger">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST" action="{{ route('workspace.update', $workspace->id) }}" enctype="multipart/form-data">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            @include('partials.textfield', ['field' => 'name', 'label' => __('document.name'), 'required' => true])

                            @include('partials.textfield', ['field' => 'street', 'label' => __('address.street'), 'required' => true])
                            @include('partials.textfield', ['field' => 'zip', 'label' => __('address.zip'), 'required' => true])
                            @include('partials.textfield', ['field' => 'city', 'label' => __('address.city'), 'required' => true])
                            @include('partials.textfield', ['field' => 'land', 'label' => __('address.land'), 'required' => false])

                            <hr>

                            @include('partials.textfield', ['field' => 'signature[0][name]', 'label' => __('signature.0.name'), 'required' => true])
                            @include('partials.textfield', ['field' => 'signature[0][representation]', 'label' => __('signature.0.representation'), 'required' => false])
                            @include('partials.textfield', ['field' => 'signature[0][function_option]', 'label' => __('signature.0.function_option'), 'required' => false])
                            @include('partials.file', ['field' => 'signature[0][file]', 'label' => __('signature.0.file'), 'required' => false])
                            @if(isset($workspace->signature[0]) && $workspace->signature[0]->hasMedia('signature'))
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-6">
                                        <img src="{{ $workspace->signature[0]->getFirstMedia('signature')->getUrl() }}" width="100">
                                    </div>
                                </div>
                            @endif

                            <hr>

                            @include('partials.textfield', ['field' => 'signature[1][name]', 'label' => __('signature.1.name'), 'required' => true])
                            @include('partials.textfield', ['field' => 'signature[1][representation]', 'label' => __('signature.1.representation'), 'required' => false])
                            @include('partials.textfield', ['field' => 'signature[1][function_option]', 'label' => __('signature.1.function_option'), 'required' => false])
                            @include('partials.file', ['field' => 'signature[1][file]', 'label' => __('signature.1.file'), 'required' => false])
                            @if(isset($workspace->signature[1]) && $workspace->signature[1]->hasMedia('signature'))
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-6">
                                        <img src="{{ $workspace->signature[1]->getFirstMedia('signature')->getUrl() }}" width="100">
                                    </div>
                                </div>
                            @endif

                            <hr>

                            @include('partials.textfield', ['field' => 'signature[2][name]', 'label' => __('signature.2.name'), 'required' => true])
                            @include('partials.textfield', ['field' => 'signature[2][representation]', 'label' => __('signature.2.representation'), 'required' => false])
                            @include('partials.textfield', ['field' => 'signature[2][function_option]', 'label' => __('signature.2.function_option'), 'required' => false])
                            @include('partials.file', ['field' => 'signature[2][file]', 'label' => __('signature.2.file'), 'required' => false])
                            @if(isset($workspace->signature[2]) && $workspace->signature[2]->hasMedia('signature'))
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-6">
                                        <img src="{{ $workspace->signature[2]->getFirstMedia('signature')->getUrl() }}" width="100">
                                    </div>
                                </div>
                            @endif

                            <hr>

                            @include('partials.textfield', ['field' => 'dental_assistant', 'label' => __('workspace.dental_assistant'), 'required' => false])
                            @include('partials.textfield', ['field' => 'medical_officers', 'label' => __('workspace.medical_officers'), 'required' => true])
                            @include('partials.textfield', ['field' => 'medical_device', 'label' => __('workspace.medical_device'), 'required' => true])
                            @include('partials.textfield', ['field' => 'security_officers', 'label' => __('workspace.security_officers'), 'required' => true])
                            @include('partials.textfield', ['field' => 'laser_protection', 'label' => __('workspace.laser_protection'), 'required' => true])
                            @include('partials.textfield', ['field' => 'leading_zfa', 'label' => __('workspace.leading_zfa'), 'required' => true])
                            @include('partials.textfield', ['field' => 'leading_zfa_representative', 'label' => __('workspace.leading_zfa_representative'), 'required' => false])
                            @include('partials.textfield', ['field' => 'zfas', 'label' => __('workspace.zfas'), 'required' => false])
                            @include('partials.textfield', ['field' => 'zfas_representative', 'label' => __('workspace.zfas_representative'), 'required' => false])
                            @include('partials.textfield', ['field' => 'reception', 'label' => __('workspace.reception'), 'required' => false])
                            @include('partials.textfield', ['field' => 'reception_representation', 'label' => __('workspace.reception_representation'), 'required' => false])
                            @include('partials.textfield', ['field' => 'accounting', 'label' => __('workspace.accounting'), 'required' => false])
                            @include('partials.textfield', ['field' => 'billing_representative', 'label' => __('workspace.billing_representative'), 'required' => false])
                            @include('partials.textfield', ['field' => 'practice_laboratory', 'label' => __('workspace.practice_laboratory'), 'required' => false])
                            @include('partials.textfield', ['field' => 'practice_laboratory_representative', 'label' => __('workspace.practice_laboratory_representative'), 'required' => false])
                            @include('partials.textfield', ['field' => 'staff', 'label' => __('workspace.staff'), 'required' => false])
                            @include('partials.textfield', ['field' => 'personal_representation', 'label' => __('workspace.personal_representation'), 'required' => false])
                            @include('partials.textfield', ['field' => 'financial_accounting', 'label' => __('workspace.financial_accounting'), 'required' => false])
                            @include('partials.textfield', ['field' => 'financial_accounting_representative', 'label' => __('workspace.financial_accounting_representative'), 'required' => false])
                            @include('partials.textfield', ['field' => 'purchase', 'label' => __('workspace.purchase'), 'required' => false])
                            @include('partials.textfield', ['field' => 'shopping_representation', 'label' => __('workspace.shopping_representation'), 'required' => false])
                            @include('partials.textfield', ['field' => 'it_organization', 'label' => __('workspace.it_organization'), 'required' => false])
                            @include('partials.textfield', ['field' => 'it_organization_representative', 'label' => __('workspace.it_organization_representative'), 'required' => false])

                            @include('partials.checkbox', ['field' => 'terms', 'label' => '<a href="storage/' . \App\Term::orderBy('updated_at', 'desc')->first()->file . '">' . __('workspace.terms') . '</a>', 'required' => true])

                            @include('partials.submitfield', ['label' => __('document.submit')])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
