@php $dotField = str_replace(['[', ']'], ['.', ''], $field) @endphp

<div class="form-group{{ $errors->has($dotField) ? ' has-error' : '' }}">
    <div class="col-md-offset-4 col-md-6 checkbox">
        <label>
            <input type="checkbox" name="{{ $field  }}" value="1" {{ (old($dotField) ?: (isset($value) ? $value : '')) ? 'checked="checked"' : '' }}  {{ isset($attributes) ? $attributes : '' }} />
            {!! $label !!} {{ $required ? '*' : '' }}
        </label>
        @if ($errors->has($dotField))
            <span class="help-block">
                <strong>{{ $errors->first($dotField) }}</strong>
            </span>
        @endif
    </div>
</div>