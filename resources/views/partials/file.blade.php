@php $dotField = str_replace(['[', ']'], ['.', ''], $field) @endphp

<div class="form-group{{ $errors->has($dotField) ? ' has-error' : '' }}">
    <label for="{{ $field  }}" class="col-md-4 control-label">{{ $label }} {{ $required ? '*' : '' }}</label>

    <div class="col-md-6">
        <input type="file" id="{{ $field }}" type="text" class="form-control" name="{{ $field  }}" value="{{ old($dotField) ?: (isset($value) ? $value : '') }}"  {{ isset($attributes) ? $attributes : '' }}>

        @if ($errors->has($dotField))
            <span class="help-block">
                <strong>{{ $errors->first($dotField) }}</strong>
            </span>
        @endif
    </div>
</div>