@php $dotField = str_replace(['[', ']'], ['.', ''], $field) @endphp

<div class="form-group{{ $errors->has($dotField) ? ' has-error' : '' }}">
    <label for="{{ $field  }}" class="col-md-4 control-label">{{ $label }} {{ $required ? '*' : '' }}</label>

    <div class="col-md-6">
        <textarea id="{{ $field }}" type="text" class="form-control richTextBox" name="{{ $field  }}"  {{ isset($attributes) ? $attributes : '' }}>{{ old($dotField) ?: (isset($value) ? $value : '') }}</textarea>

        @if ($errors->has($dotField))
            <span class="help-block">
                <strong>{{ $errors->first($dotField) }}</strong>
            </span>
        @endif
    </div>
</div>