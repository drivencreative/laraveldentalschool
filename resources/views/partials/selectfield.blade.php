@php $dotField = str_replace(['[', ']'], ['.', ''], $field) @endphp

<div class="form-group{{ $errors->has($dotField) ? ' has-error' : '' }}">
    <label for="{{ $field }}" class="col-md-4 control-label">{{ $label }} {{ $required ? '*' : '' }}</label>

    <div class="col-md-6">
        <select class="form-control" id="{{ $field }}" name="{{ $field }}"  {{ isset($attributes) ? $attributes : '' }}>
            @foreach($options as $key => $option)
                <option value="{{ $key }}" {{ (old($dotField) ?: (isset($value) ? $value : '')) == $key ? 'selected' : '' }}>{{ $option }}</option>
            @endforeach
        </select>

        @if ($errors->has($dotField))
            <span class="help-block">
                <strong>{{ $errors->first($dotField) }}</strong>
            </span>
        @endif
    </div>
</div>