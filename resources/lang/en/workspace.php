<?php

return [

    'accept_terms'                          => 'Please accept Terms and Conditions!',
    'terms'                                 => 'Terms and Conditions',
    'dental_assistant'                      => 'Assistant dentists',
    'medical_officers'                      => 'Medical device representative(s)',
    'medical_device'                        => 'Medical Product Representative(s)',
    'security_officers'                     => 'Security officer(s)',
    'laser_protection'                      => 'Laser Safety Officer(s)',
    'leading_zfa'                           => 'Leading ZFA',
    'leading_zfa_representative'            => 'Leading ZFA representation',
    'zfas'                                  => 'ZFAs',
    'zfas_representative'                   => 'ZFAs-representation',
    'reception'                             => 'Reception',
    'reception_representation'              => 'Reception Representation',
    'accounting'                            => 'Billing',
    'billing_representative'                => 'Billing-Vertretung',
    'practice_laboratory'                   => 'Practice Laboratory',
    'practice_laboratory_representative'    => 'Practice Laboratory Representation',
    'staff'                                 => 'Personal',
    'personal_representation'               => 'Personal Representation',
    'financial_accounting'                  => 'Financial accounting',
    'financial_accounting_representative'   => 'Financial accounting Representation',
    'purchase'                              => 'Purchase',
    'shopping_representation'               => 'Purchasing representation',
    'it_organization'                       => 'IT Organization',
    'it_organization_representative'        => 'IT Organization Representation',

];