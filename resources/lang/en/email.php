<?php

return [

    'You are receiving this email because we received a password reset request for your account.' => 'You are receiving this email because we received a password reset request for your account.',
    'Reset Password' => 'Reset Password',
    'If you did not request a password reset, no further action is required.' => 'If you did not request a password reset, no further action is required.',
    'footer' => 'If you have trouble clicking "Reset Password" button, copy the following URL and paste it into your web browser: :actionUrl',
    'regards' => 'regards',
    'hello' =>  'Hello!',

    'cart' => 'We have received your order.',
    'orders' => 'See your previous orders.',

];