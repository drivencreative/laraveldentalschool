<?php

return [

    'no_devices_category'       => 'No devices were found in this category.',
    'new_device'                => 'New Device',
    'edit_device'               => 'Edit device in category :category',
    'initialize_devices'        => 'Initialize devices',
    'initialize'                => 'Initialize',
    'device'                    => 'Device Manager',
    'new'                       => 'New',
    'name'                      => 'Name',
    'type'                      => 'Type',
    'manufacturer'              => 'Manufacturer',
    'ce_mark'                   => 'CE-Mark',
    'serial_number'             => 'Serial Number',
    'year_of_construction'      => 'Year of Construction',
    'space'                     => 'Space',
    'number'                    => 'Number',
    'notes'                     => 'Notes',

];