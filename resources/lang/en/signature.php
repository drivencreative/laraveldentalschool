<?php

return [

    0 => [
        'name'                      => 'Name responsible dentist (vZA)',
        'representation'            => 'vZA representation',
        'function_option'           => 'Deviating from vZA',
        'file'                      => 'Signature responsible dentist (vZA)',
    ],
    1 => [
        'name'                      => 'Quality Management Officer (QMB)',
        'representation'            => 'Representation',
        'function_option'           => 'Function / Option',
        'file'                      => 'Signature (QMB) if desired',
    ],
    2 => [
        'name'                      => 'Hygienic Responsible (HygV)',
        'representation'            => 'QMB representation',
        'function_option'           => 'Deviating from the QMB',
        'file'                      => 'Signature Hygienic Responsible (HygV)',
    ],

];