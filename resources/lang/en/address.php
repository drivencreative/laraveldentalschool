<?php

return [

    'name'                      => 'Name',
    'street'                    => 'Street',
    'zip'                       => 'Zip',
    'city'                      => 'City',
    'land'                      => 'Land',

];