<?php

return [

    'header'                => 'I am already a customer',
    'text'                  => 'Have you ever ordered from us? Thank you very much for your confidence in us. On the next page you have the possibility to change your delivery address or to add a new delivery address as well as to select your payment method.',
    'profile'               => 'Profile',
    'Login'                 => 'Login',
    'login'                 => 'Login',
    'register'              => 'Register',
    'email'                 => 'E-Mail',
    'password'              => 'Password',
    'remember_me'           => 'Remember Me',
    'forgot_password'       => 'Forgot Your Password?',
    'send_password_reset'   => 'Send Password Reset Link',
    'reset_password'        => 'Reset Password',
    'logged_in'             => 'You are logged in!',
    'not_logged_in'         => 'You are not logged in!',

];