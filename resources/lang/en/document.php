<?php

return [

    'no_documents_category'     => 'No documents found for this category.',
    'links'                     => 'Links',
    'categories'                => 'Categories',
    'workspace'                 => 'Workspace',
    'document'                  => 'Document',
    'pending_documents'         => 'Pending Documents',
    'notification_documents'    => 'Notifications',
    'initialize_documents'      => 'Initialize documents',
    'initialize'                => 'Initialize',
    'preview'                   => 'Preview',
    'new'                       => [
        'file'                  => 'Upload',
        'html'                  => 'New',
        'link'                  => 'New Link',
    ],
    'name'                      => 'Name',
    'content'                   => 'Content',
    'additional_content'        => 'Additional Content',
    'file'                      => 'File',
    'link'                      => 'Url',
    'version'                   => 'Version',
    'new_document'              => 'New Document',
    'edit_document'             => 'Edit Document',
    'version_document'          => 'New Document Version',
    'sign_document'             => 'Sign Document',
    'signature'                 => 'Signature',
    'submit'                    => 'Save',
    'back'                      => 'back',
    'release_date'              => 'Release date',
    'original_version'          => 'Original Version',
    'your_version'              => 'Your version',
    'compare_version'           => [
        'apply'                 => 'Apply new content',
        'keep'                  => 'Keep my version',
    ]

];