<?php

return [

    'header'                => [
        'personal'              => 'Your personal data',
        'company'               => 'Company data',
        'address'               => 'Your address',
        'contact'               => 'Your contact information',
        'password'              => 'Password',
        'password_text'         => 'By signing up with us you will be able to shop faster, know at all times the status of your orders and always have an up to date account of your busher-made orders.',
    ],
    'please_select'         => 'Please select',
    'gender'                => 'Gender',
    'genders'               => [
        1 => 'Mr.',
        2 => 'Ms.',
    ],
    'name'                  => 'Name',
    'surname'               => 'Surname',
    'email'                 => 'E-Mail',
    'email_confirmation'    => 'Confirm E-Mail',
    'company'               => 'Company',
    'company2'              => 'Company 2',
    'company3'              => 'Company 3',
    'ust_id'                => 'USt-Id',
    'street'                => 'Street',
    'zip'                   => 'Zip',
    'city'                  => 'City',
    'land'                  => 'Land',
    'lands'                 => [
        1 => 'Germany',
        2 => 'Austria',
    ],
    'phone'                 => 'Phone',
    'fax'                   => 'Fax',
    'password'              => 'Password',
    'password_confirmation' => 'Confirm Password',
    'submit'                => 'Register',

];