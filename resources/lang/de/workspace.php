<?php

return [

    'accept_terms'                          => 'Please accept Terms and Conditions!',
    'terms'                                 => 'Terms and Conditions',
    'dental_assistant'                      => 'Assistenzzahnärzte',
    'medical_officers'                      => 'Medizinproduktebeauftragte(r)',
    'medical_device'                        => 'Medizinproduktbeauftragter(r)-Vertretung',
    'security_officers'                     => 'Sicherheitsbeauftragte(r)',
    'laser_protection'                      => 'Laserschutzbeauftragte(r)',
    'leading_zfa'                           => 'Leitende ZFA',
    'leading_zfa_representative'            => 'Leitende ZFA-Vertretung',
    'zfas'                                  => 'ZFAs',
    'zfas_representative'                   => 'ZFAs-Vertretung',
    'reception'                             => 'Rezeption',
    'reception_representation'              => 'Rezeption-Vertretung',
    'accounting'                            => 'Abrechnung',
    'billing_representative'                => 'Abrechnung-Vertretung',
    'practice_laboratory'                   => 'Praxislabor',
    'practice_laboratory_representative'    => 'Praxislabor-Vertretung',
    'staff'                                 => 'Personal',
    'personal_representation'               => 'Personal-Vertretung',
    'financial_accounting'                  => 'Finanzbuchhaltung',
    'financial_accounting_representative'   => 'Finanzbuchhaltung-Vertretung',
    'purchase'                              => 'Einkauf',
    'shopping_representation'               => 'Einkauf-Vertretung',
    'it_organization'                       => 'IT-Organisation',
    'it_organization_representative'        => 'IT-Organisation-Vertretung',

];