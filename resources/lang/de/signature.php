<?php

return [

    0 => [
        'name'                      => 'Name verantwortlicher Zahnarzt (vZA)',
        'representation'            => 'vZA-Vertretung',
        'function_option'           => 'abweichend vom vZA',
        'file'                      => 'Unterschrift verantwortlicher Zahnarzt (vZA)',
    ],
    1 => [
        'name'                      => 'Qualitätsmanagementbeauftragter (QMB)',
        'representation'            => 'Vertretung',
        'function_option'           => 'Function/Option',
        'file'                      => 'Unterschrift (QMB) soweit erwünscht',
    ],
    2 => [
        'name'                      => 'Hygieneverantwortliche (HygV)',
        'representation'            => 'QMB-Vertretung',
        'function_option'           => 'abweichend vom QMB',
        'file'                      => 'Unterschrift Hygieneverantwortliche(HygV)',
    ],

];