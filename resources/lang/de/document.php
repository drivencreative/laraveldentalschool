<?php

return [

    'no_documents_category'     => 'Es wurden keine Dokumente für diese Kategorie gefunden.',
    'links'                     => 'Links',
    'categories'                => 'Kategorie',
    'workspace'                 => 'Workspace',
    'document'                  => 'Dokumente',
    'pending_documents'         => 'Ausstehende Dokumente',
    'notification_documents'    => 'Benachrichtigungen',
    'initialize_documents'      => 'Dokumente Initialisieren',
    'initialize'                => 'Initialize',
    'preview'                   => 'Vorschau',
    'new'                       => [
        'file'                  => 'Upload',
        'html'                  => 'Neu',
        'link'                  => 'Neu Link',
    ],
    'name'                      => 'Name',
    'content'                   => 'Inhalt',
    'additional_content'        => 'Zusätzlicher Inhalt',
    'file'                      => 'Upload',
    'link'                      => 'URL',
    'version'                   => 'Version',
    'new_document'              => 'Neues Dokument',
    'edit_document'             => 'Dokument bearbeiten',
    'version_document'          => 'Neue Dokumentversion',
    'sign_document'             => 'Dokument unterschreiben',
    'signature'                 => 'Unterschrift',
    'submit'                    => 'Speichern',
    'back'                      => 'zurück',
    'release_date'              => 'Freigabe Datum',
    'original_version'          => 'Vorlagen-Version',
    'your_version'              => 'Ihre Version',
    'compare_version'           => [
        'apply'                 => 'neue Inhalte Übernehmen',
        'keep'                  => 'meine Version behalten',
    ]

];