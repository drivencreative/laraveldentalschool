<?php

return [

    'You are receiving this email because we received a password reset request for your account.' => 'Sie erhalten diese E-Mail, weil wir eine Anfrage zum Zurücksetzen des Passworts für Ihr Konto erhalten haben.',
    'Reset Password' => 'Passwort zurücksetzen',
    'If you did not request a password reset, no further action is required.' => 'Wenn Sie kein Zurücksetzen des Kennworts angefordert haben, sind keine weiteren Maßnahmen erforderlich.',
    'footer' =>'Wenn Sie Probleme beim Klicken auf "Passwort zurücksetzen" Taste, Kopieren Sie die folgende URL und fügen Sie sie in Ihren Webbrowser ein: :actionUrl',
    'regards' => 'Grüße',
    'hello' =>  'Hallo!',

    'cart' => 'Wir haben Ihre Bestellung erhalten.',
    'orders' => 'Sehen Sie Ihre früheren Bestellungen.',

];