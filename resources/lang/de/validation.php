<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Das :attribute muss akzeptiert werden.',
    'active_url'           => 'Das :attribute ist keine gültige URL.',
    'after'                => 'Das :attribute muss ein Datum sein, nachdem :date.',
    'after_or_equal'       => 'Das :attribute muss ein Datum nach oder gleich sein :date.',
    'alpha'                => 'Das :attribute darf nur Buchstaben enthalten.',
    'alpha_dash'           => 'Das :attribute darf nur Buchstaben, Zahlen und Bindestriche enthalten.',
    'alpha_num'            => 'Das :attribute darf nur Buchstaben und Zahlen enthalten.',
    'array'                => 'Das :attribute muss ein Array sein.',
    'before'               => 'Das :attribute muss ein Datum vorher sein :date.',
    'before_or_equal'      => 'Das :attribute muss ein Datum vor oder gleich sein :date.',
    'between'              => [
        'numeric' => 'Das :attribute muss zwischen :min und :max.',
        'file'    => 'Das :attribute muss zwischen :min und :max Kilobyte.',
        'string'  => 'Das :attribute muss zwischen :min und :max Charakter.',
        'array'   => 'Das :attribute muss zwischen :min und :max Artikel.',
    ],
    'boolean'              => 'Das :attribute Feld muss wahr oder falsch sein.',
    'confirmed'            => 'Das :attribute Bestätigung stimmt nicht überein.',
    'date'                 => 'Das :attribute ist kein gültiges Datum.',
    'date_format'          => 'Das :attribute stimmt nicht mit dem Format überein :format.',
    'different'            => 'Das :attribute und :other muss anders sein.',
    'digits'               => 'Das :attribute muss sein :digits Ziffern.',
    'digits_between'       => 'Das :attribute muss zwischen :min und :max Ziffern.',
    'dimensions'           => 'Das :attribute hat ungültige Bildmaße.',
    'distinct'             => 'Das :attribute Feld hat einen doppelten Wert.',
    'email'                => 'Das :attribute muss eine gültige E-Mail-Adresse sein.',
    'exists'               => 'Das ausgewählt :attribute ist ungültig.',
    'file'                 => 'Das :attribute muss eine Datei sein.',
    'filled'               => 'Das :attribute Feld muss einen Wert haben.',
    'image'                => 'Das :attribute muss ein Bild sein.',
    'in'                   => 'Das ausgewählt :attribute ist ungültig.',
    'in_array'             => 'Das :attribute Feld existiert nicht in :other.',
    'integer'              => 'Das :attribute muss eine ganze Zahl sein.',
    'ip'                   => 'Das :attribute muss eine gültige IP-Adresse sein.',
    'ipv4'                 => 'Das :attribute muss eine gültige IPv4-Adresse sein.',
    'ipv6'                 => 'Das :attribute muss eine gültige IPv6-Adresse sein.',
    'json'                 => 'Das :attribute muss ein gültiger JSON-String sein.',
    'max'                  => [
        'numeric' => 'Das :attribute darf nicht größer sein als :max.',
        'file'    => 'Das :attribute darf nicht größer sein als :max Kilobyte.',
        'string'  => 'Das :attribute darf nicht größer sein als :max Charakter.',
        'array'   => 'Das :attribute darf nicht mehr als haben :max Artikel.',
    ],
    'mimes'                => 'Das :attribute muss eine Datei vom Typ sein: :values.',
    'mimetypes'            => 'Das :attribute muss eine Datei vom Typ sein: :values.',
    'min'                  => [
        'numeric' => 'Das :attribute muss mindestens :min.',
        'file'    => 'Das :attribute muss mindestens :min Kilobyte.',
        'string'  => 'Das :attribute muss mindestens :min Charakter.',
        'array'   => 'Das :attribute muss mindestens haben :min Artikel.',
    ],
    'not_in'               => 'Das ausgewählt :attribute ist ungültig.',
    'numeric'              => 'Das :attribute muss eine Nummer sein.',
    'present'              => 'Das :attribute Feld muss vorhanden sein.',
    'regex'                => 'Das :attribute Format ist ungültig.',
    'required'             => 'Das :attribute Feld ist erforderlich.',
    'required_if'          => 'Das :attribute Feld ist erforderlich, wenn :other ist :value.',
    'required_unless'      => 'Das :attribute Feld ist erforderlich, es sei denn :other ist in :values.',
    'required_with'        => 'Das :attribute Feld ist erforderlich, wenn :values ist anwesend.',
    'required_with_all'    => 'Das :attribute Feld ist erforderlich, wenn :values ist anwesend.',
    'required_without'     => 'Das :attribute Feld ist erforderlich, wenn :values ist nicht anwesend.',
    'required_without_all' => 'Das :attribute Feld ist erforderlich, wenn keiner von :values sind anwesend.',
    'same'                 => 'Das :attribute und :other muss übereinstimmen.',
    'size'                 => [
        'numeric' => 'Das :attribute muss sein :size.',
        'file'    => 'Das :attribute muss sein :size Kilobyte.',
        'string'  => 'Das :attribute muss sein :size Charakter.',
        'array'   => 'Das :attribute muss enthalten :size Artikel.',
    ],
    'string'               => 'Das :attribute Muss eine Zeichenfolge sein.',
    'timezone'             => 'Das :attribute muss eine gültige Zone sein.',
    'unique'               => 'Das :attribute wurde bereits genommen.',
    'uploaded'             => 'Das :attribute konnte nicht hochgeladen werden.',
    'url'                  => 'Das :attribute Format ist ungültig.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'benutzerdefinierte-nachricht',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'type'                          => 'Typ',
        'name'                          => 'Vorname',
        'email'                         => 'E-Mail-Adresse',
        'password'                      => 'Passwort',
        'content'                       => 'Inhalt',
        'file'                          => 'Datei',
        'address' => [
            'name'                      => 'Name',
            'street'                    => 'Street',
            'zip'                       => 'Zip',
            'city'                      => 'City',
            'land'                      => 'Land',
        ],
        'sign'                          => 'Schild',
        'signature' => [
            0 => [
                'name'                  => 'Name verantwortlicher Zahnarzt (vZA)',
                'file'                  => 'Unterschrift verantwortlicher Zahnarzt (vZA)',
            ],
            1 => [
                'name'                  => 'Qualitätsmanagementbeauftragter (QMB)',
                'file'                  => 'Unterschrift (QMB) soweit erwünscht',
            ],
            2 => [
                'name'                  => 'Hygieneverantwortliche (HygV)',
                'file'                  => 'Unterschrift Hygieneverantwortliche(HygV)',
            ],
        ],
        'medical_officers'              => 'Medizinproduktebeauftragte(r)',
        'medical_device'                => 'Medizinproduktbeauftragter(r)-Vertretung',
        'security_officers'             => 'Sicherheitsbeauftragte(r)',
        'laser_protection'              => 'Laserschutzbeauftragte(r)',
        'leading_zfa'                   => 'Leitende ZFA',
        'terms'                         => 'Terms and Conditions',
        'manufacturer'                  => 'Hersteller',
    ],

];
