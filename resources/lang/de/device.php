<?php

return [

    'no_devices_category'       => 'Es wurden keine Geräte in dieser Kategorie gefunden.',
    'new_device'                => 'Neues Geräte',
    'edit_device'               => 'Gerät in der Kategorie :category bearbeiten',
    'initialize_devices'        => 'Gerät Initialisieren',
    'initialize'                => 'Initialize',
    'device'                    => 'Gerätemanager',
    'new'                       => 'Neu',
    'name'                      => 'Name',
    'type'                      => 'Typ',
    'manufacturer'              => 'Hersteller',
    'ce_mark'                   => 'CE-Zeichen',
    'serial_number'             => 'Seriennummer',
    'year_of_construction'      => 'Baujahr',
    'space'                     => 'Raum',
    'number'                    => 'Anzahl',
    'notes'                     => 'Notes',

];