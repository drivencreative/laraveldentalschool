<?php

return [

    'header'                => 'Ich bin bereits Kunde',
    'text'                  => 'Sie haben schon einmal bei uns bestellt? Herzlichen Dank, dass Sie uns ein weiteres Mal Ihr Vertrauen schenken. Auf der nächsten Seite haben Sie die Möglichkeit Ihre Lieferanschrift zu ändern, oder eine neue Lieferanschrift hinzuzufügen, sowie Ihre Zahlungsart auszuwählen.',
    'profile'               => 'Profil verwalten',
    'Login'                 => 'Login',
    'login'                 => 'Anmeldung',
    'register'              => 'Register',
    'email'                 => 'E-Mail-Adresse',
    'password'              => 'Passwort',
    'remember_me'           => 'Erinnere dich an mich',
    'forgot_password'       => 'Passwort vergessen?',
    'send_password_reset'   => 'Link zum Zurücksetzen des Passworts senden',
    'reset_password'        => 'Passwort zurücksetzen',
    'logged_in'             => 'Sie sind eingeloggt',
    'not_logged_in'         => 'Sie sind nicht eingeloggt',

];