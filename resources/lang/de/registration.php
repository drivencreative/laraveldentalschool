<?php

return [

    'header'                => [
        'personal'              => 'Ihre persönlichen Daten',
        'company'               => 'Firmendaten',
        'address'               => 'Ihre Adresse',
        'contact'               => 'Ihre Kontaktinformationen',
        'password'              => 'Passwort',
        'password_text'         => 'Durch Ihre Anmeldung bei uns sind Sie in der Lage schneller zu bestellen, kennen jederzeit den Status Ihrer Bestellungen und haben immer eine aktuelle Übersicht über Ihre bisher getätigten Bestellungen.',
    ],
    'please_select'         => 'Bitte auswählen',
    'gender'                => 'Anrede',
    'genders'               => [
        1 => 'Herr',
        2 => 'Frau',
    ],
    'name'                  => 'Vorname',
    'surname'               => 'Nachname',
    'email'                 => 'E-Mail-Adresse',
    'email_confirmation'    => 'E-Mail-Adresse bestätigen',
    'company'               => 'Firmenname',
    'company2'              => 'Zeile 2',
    'company3'              => 'Zeile 3',
    'ust_id'                => 'USt-Id',
    'street'                => 'Straße',
    'zip'                   => 'Postleitzahl',
    'city'                  => 'Stadt',
    'land'                  => 'Land',
    'lands'                 => [
        1 => 'Deutschland',
        2 => 'Österreich',
    ],
    'phone'                 => 'Telefon',
    'fax'                   => 'Fax',
    'password'              => 'Ihr neues Passwort',
    'password_confirmation' => 'Passwort bestätigen',
    'submit'                => 'Weiter',

];