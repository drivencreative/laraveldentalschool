<?php

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::middleware(['auth', 'workspace'])->group(function () {
    Route::resource('document/initialize', '\App\Http\Controllers\Document\InitializeController', ['only' => ['create', 'store']]);
    Route::resource('document/version', '\App\Http\Controllers\Document\VersionController', ['only' => ['edit', 'update']]);
    Route::resource('document/sign', '\App\Http\Controllers\Document\SignController', ['only' => ['index', 'edit', 'update']]);
    Route::resource('document/notification', '\App\Http\Controllers\Document\NotificationController', ['only' => ['index', 'edit', 'update']]);
    Route::resource('document', 'DocumentController');
    Route::resource('device', 'DeviceController');
    Route::resource('device/initialize', '\App\Http\Controllers\Device\InitializeController', ['as' => 'device', 'only' => ['create', 'store']]);
    Route::resource('workspace', 'WorkspaceController', ['only' => ['index', 'update']]);
});