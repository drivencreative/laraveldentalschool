<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentfile', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('document_id');

            $table->integer('version')->unsigned()->default(0);;
            $table->longText('content')->nullable();
            $table->longText('additional_content')->nullable();

            $table->dateTime('signed')->nullable();
            $table->string('signature')->nullable();
            $table->integer('signature_index')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentfile');
    }
}
