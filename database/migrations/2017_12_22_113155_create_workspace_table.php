<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkspaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workspace', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->string('street')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('land')->nullable();

            $table->boolean('master')->default(false);
            $table->boolean('initialized')->default(false);
            $table->boolean('terms')->default(false);

            $table->string('dental_assistant')->nullable();
            $table->string('medical_officers')->nullable();
            $table->string('medical_device')->nullable();
            $table->string('security_officers')->nullable();
            $table->string('laser_protection')->nullable();
            $table->string('leading_zfa')->nullable();
            $table->string('leading_zfa_representative')->nullable();
            $table->string('zfas')->nullable();
            $table->string('zfas_representative')->nullable();
            $table->string('reception')->nullable();
            $table->string('reception_representation')->nullable();
            $table->string('accounting')->nullable();
            $table->string('billing_representative')->nullable();
            $table->string('practice_laboratory')->nullable();
            $table->string('practice_laboratory_representative')->nullable();
            $table->string('staff')->nullable();
            $table->string('personal_representation')->nullable();
            $table->string('financial_accounting')->nullable();
            $table->string('financial_accounting_representative')->nullable();
            $table->string('purchase')->nullable();
            $table->string('shopping_representation')->nullable();
            $table->string('it_organization')->nullable();
            $table->string('it_organization_representative')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workspace');
    }
}
