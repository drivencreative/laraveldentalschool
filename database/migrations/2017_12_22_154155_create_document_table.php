<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('workspace_id')->unsigned()->default(0);
            $table->integer('category_id')->unsigned()->default(0);

            $table->string('name');
            $table->string('type');
            $table->integer('based_on_version')->unsigned()->default(0)->nullable();
            $table->integer('based_on')->unsigned()->default(0)->nullable();
            $table->boolean('notify')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
