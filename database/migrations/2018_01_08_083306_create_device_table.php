<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('workspace_id')->unsigned()->default(0);
            $table->integer('category_id')->unsigned()->default(0);

            $table->string('type');
            $table->string('name');
            $table->string('manufacturer');
            $table->string('ce_mark')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('year_of_construction')->nullable();
            $table->string('space')->nullable();
            $table->integer('number')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device');
    }
}
